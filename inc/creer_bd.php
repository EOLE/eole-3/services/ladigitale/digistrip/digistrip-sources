<?php

session_start();

require 'headers.php';

if (!empty($_POST['titre']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$bd = uniqid('', false);
	$titre = $_POST['titre'];
	$question = $_POST['question'];
	$reponse = password_hash(strtolower($_POST['reponse']), PASSWORD_DEFAULT);
	$donnees = '';
	$date = date('Y-m-d H:i:s');
	$stmt = $db->prepare('INSERT INTO digistrip_bd (url, titre, question, reponse, donnees, date) VALUES (:url, :titre, :question, :reponse, :donnees, :date)');
	if ($stmt->execute(array('url' => $bd, 'titre' => $titre, 'question' => $question, 'reponse' => $reponse, 'donnees' => $donnees, 'date' => $date))) {
		$_SESSION['digistrip'][$bd]['reponse'] = $reponse;
		echo $bd;
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
