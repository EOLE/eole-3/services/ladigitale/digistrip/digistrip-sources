<?php

session_start();

require 'headers.php';

if (!empty($_POST['bd']) && !empty($_POST['question']) && !empty($_POST['reponse']) && !empty($_POST['nouvellequestion']) && !empty($_POST['nouvellereponse'])) {
	require 'db.php';
	$bd = $_POST['bd'];
	$question = $_POST['question'];
	$reponse = strtolower($_POST['reponse']);
	$stmt = $db->prepare('SELECT question, reponse FROM digistrip_bd WHERE url = :url');
	if ($stmt->execute(array('url' => $bd))) {
		$resultat = $stmt->fetchAll();
		$questionSecrete = '';
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else {
			$questionSecrete = $resultat[0]['question'];
			$reponseSecrete = $resultat[0]['reponse'];
			if ($question === $questionSecrete && password_verify($reponse, $reponseSecrete)) {
				$nouvellequestion = $_POST['nouvellequestion'];
				$nouvellereponse = password_hash(strtolower($_POST['nouvellereponse']), PASSWORD_DEFAULT);
				$stmt = $db->prepare('UPDATE digistrip_bd SET question = :nouvellequestion, reponse = :nouvellereponse WHERE url = :url');
				if ($stmt->execute(array('nouvellequestion' => $nouvellequestion, 'nouvellereponse' => $nouvellereponse, 'url' => $bd))) {
					$_SESSION['digistrip'][$bd]['reponse'] = $nouvellereponse;
					echo 'acces_modifie';
				} else {
					echo 'erreur';
				}
			} else {
				echo 'non_autorise';
			}
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
