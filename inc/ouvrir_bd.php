<?php

session_start();

require 'headers.php';

if (!empty($_POST['id']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$bd = $_POST['id'];
	$question = $_POST['question'];
	$reponse = strtolower($_POST['reponse']);
	$stmt = $db->prepare('SELECT question, reponse FROM digistrip_bd WHERE url = :url');
	if ($stmt->execute(array('url' => $bd))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else {
			$questionSecrete = $resultat[0]['question'];
			$reponseSecrete = $resultat[0]['reponse'];
			if ($question === $questionSecrete && password_verify($reponse, $reponseSecrete)) {
				$_SESSION['digistrip'][$bd]['reponse'] = $reponseSecrete;
				$type = $_POST['type'];
				if ($type === 'api' && !isset($_SESSION['digistrip'][$bd]['digidrive'])) {
					$_SESSION['digistrip'][$bd]['digidrive'] = 1;
				}
				echo 'bd_debloquee';
			} else {
				echo 'non_autorise';
			}
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
