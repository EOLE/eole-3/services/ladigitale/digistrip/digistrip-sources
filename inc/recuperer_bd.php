<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digistrip'][$id]['reponse'])) {
		$reponse = $_SESSION['digistrip'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digistrip_bd WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($bd = $stmt->fetchAll()) {
			$admin = false;
			if (count($bd, COUNT_NORMAL) > 0 && $bd[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $bd[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digistrip'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digistrip'][$id]['digidrive'];
			}
			echo json_encode(array('titre' => $bd[0]['titre'], 'donnees' => $donnees, 'admin' =>  $admin, 'digidrive' => $digidrive));
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
