<?php

session_start();

require 'headers.php';

if (!empty($_FILES['blob']) && !empty($_POST['bd'])) {
	$bd = $_POST['bd'];
	$extension = pathinfo($_FILES['blob']['name'], PATHINFO_EXTENSION);
	if (!file_exists('../fichiers/' . $bd)) {
		mkdir('../fichiers/' . $bd, 0775, true);
	}
	$nom = hash('md5', $_FILES['blob']['tmp_name']) . time() . '.' . $extension;
	$chemin = '../fichiers/' . $bd . '/' . $nom;
	if (move_uploaded_file($_FILES['blob']['tmp_name'], $chemin)) {
		echo $nom;
	} else {
		echo 'erreur';
	}
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
