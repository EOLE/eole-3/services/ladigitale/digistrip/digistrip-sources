<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	$bd = $_POST['id'];
	unset($_SESSION['digistrip'][$bd]['reponse']);
	echo 'session_terminee';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
