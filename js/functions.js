// Tirer au sort un personnage
function tirerAuSort(tableau) {
    // Génère un index aléatoire
    var indexAleatoire = Math.floor(Math.random() * tableau.length);  
    // Retourne le personnage correspondant à l'index
    return tableau[indexAleatoire];
 }

 // À propos
function a_propos(){
    texte_a_propos.classList.remove('hide');
    dark.style.display='block';
}
  
// Sortir du à propos
function quitter(){
    texte_a_propos.classList.add('hide');
    dark.style.display='none';
}

// Fonctionnement du cadenas
function change_lock() {
	if (lock){
		bouton_cadenas.classList.remove('annuler');
		document.querySelectorAll('.vignette, .bande').forEach(element => {
			element.classList.remove('hidden');
			ns_resize='ns-resize';
			ew_resize='ew-resize';
		});
	} else {
		bouton_cadenas.classList.add('annuler');
		document.querySelectorAll('.vignette, .bande').forEach(element => {
			element.classList.add('hidden');
			ns_resize=ew_resize='defaut';
		});
	}
	lock=!lock;
}

// Afficher / Masquer le titre de la BD
function change_titre() {
    if (titre_bd.classList.contains('hide')){
      titre_bd.style.fontFamily=police_family;
      titre_bd.style.fontSize=police_size+'px';
      titre_bd.classList.remove('hide');
    } else {
      titre_bd.classList.add('hide');
    }
    maj_credits_police();
}

// Convertir la couleur RGB en format hexadécimal
function rgbToHex(rgb) {
    if (rgb.startsWith("#")){
      	return rgb;
    } else {  
		let values = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		let hexColor = "#" + componentToHex(values[1]) + componentToHex(values[2]) + componentToHex(values[3]);  
		return hexColor;
    }
}
  
function componentToHex(c) {
    let hex = parseInt(c, 10).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

// Calculer le ratio largeur / hauteur d'une vignette
function getRatio(vignette){
    let largeurVignette = vignette.offsetWidth-6;
    let hauteurVignette = vignette.offsetHeight-6;
    let ratio = largeurVignette/hauteurVignette;
    return ratio;
}
  
// Récupérer le background-size d'une vignette
function getbackgroundSizeActuel(vignette){
    let style = window.getComputedStyle(vignette);
    let backgroundSizeString = style.getPropertyValue('background-size');
  
    if (backgroundSizeString==='cover'){
		if (getRatio(vignette)>=1) {
			backgroundSizeActuel=100;
		} else {
			backgroundSizeActuel=100/getRatio(vignette);
		}
    } else {  
    	let valeurs = backgroundSizeString.split(' ');
		backgroundSizeActuel=parseFloat(valeurs[0]);
    }
    return backgroundSizeActuel;
}

function zoom(vignette,facteur){
    let backgroundSizeActuel=getbackgroundSizeActuel(vignette.firstChild);
    let hauteurVignette=vignette.offsetHeight-6;
    let largeurVignette=vignette.offsetWidth-6;
  
    if (facteur>0){ // Zoom +
      	vignette.firstChild.style.backgroundSize=backgroundSizeActuel+3+'%';
    } else { // Zoom -
		if (backgroundSizeActuel-3<100) { // Éviter un background inférieur à 100%
			vignette.firstChild.style.backgroundSize='100%';
		} else {
			let largeur_prevue=(backgroundSizeActuel-3)*largeurVignette/100;
			if (largeur_prevue>hauteurVignette) {  // Éviter un background inférieur à la hauteur
				vignette.firstChild.style.backgroundSize=backgroundSizeActuel-3+'%';
			}
		}
    }
    sauvegarde();
}
  
// Suppression manuelle d'une vignette
function supprime_vignette(vignette){
	let index1=cherche_vignette(vignette)[0];
	let index2=cherche_vignette(vignette)[1];
	if (liste_vignettes[index1].length==1){
		supprime_bande(vignette.parentNode,index1);
	} else {
		let largeur_vignette_a_supprimer=parseFloat(document.defaultView.getComputedStyle(vignette).width, 10)+16;
		vignette.click=null;
		clicked=null;
		supprElementSurVignette(vignette);
		vignette.remove();
		let vignette_a_agrandir;

		if (index2===liste_vignettes[index1].length-1){
			vignette_a_agrandir=liste_vignettes[index1][index2-1];
		} else {
			vignette_a_agrandir=liste_vignettes[index1][index2+1];
		}
		let largeur_vignette_a_agrandir = parseFloat(document.defaultView.getComputedStyle(vignette_a_agrandir).width, 10);
		let nouvelle_largeur = largeur_vignette_a_agrandir + largeur_vignette_a_supprimer;
		vignette_a_agrandir.style.width=nouvelle_largeur+'px';            
	}

	// Suppression de la vignette dans la liste
	liste_vignettes[index1].splice(index2,1);

	sauvegarde();
}

// Recherche de l'index d'une vignette dans liste_vignettes (tableau à deux dimensions)
function cherche_vignette(vignette_a_chercher){  
	let index1;
	let index2;
	let num_bande=0;
	liste_vignettes.forEach(function (bande){
		let num_vignette=0;
		liste_vignettes[num_bande].forEach(function(vignette){
			if (vignette===vignette_a_chercher){index1=num_bande;index2=num_vignette;}
			num_vignette+=1;
		});
		num_bande+=1;
	});  
	return [index1,index2];
}

function change_couleur(vignette,couleur){
	vignette.style.backgroundColor = couleur;
	
	sauvegarde();
}
  
function getElementsSurVignette(vignette){
	const objets = document.querySelectorAll('.draggable');
	// Obtenir les dimensions de la vignette
	const vignetteRect = vignette.getBoundingClientRect();
	// Créer une liste temporaire
	let liste_elementsSurVignette = [];  
	// Vérifier si les éléments en position absolute sont entièrement à l'intérieur de la vignette
	objets.forEach(element => {
		const elementRect = element.getBoundingClientRect();  
		// Vérifier si l'élément est entièrement à l'intérieur de la vignette
		if (
			elementRect.top >= vignetteRect.top &&
			elementRect.bottom <= vignetteRect.bottom &&
			elementRect.left >= vignetteRect.left &&
			elementRect.right <= vignetteRect.right
		) {
			// Ajouter l'élément à une liste
			liste_elementsSurVignette.push(element);
		}
	});

	return liste_elementsSurVignette;
}
  
// Suppression des éléments de la vignette en cours
function supprElementSurVignette(vignette){
	const liste = getElementsSurVignette(vignette);
	liste.forEach(element => {
		maj_controles(element,'suppr');
		element.remove();
	});
} 

// Suppression manuelle d'une bande
function supprime_bande(bande,index){
	if (bande){
		bande.remove();
		liste_bandes.splice(index,1);
		liste_vignettes.splice(index,1);
	} else {
		liste_bandes[liste_bandes.length-1].remove();
		liste_bandes.pop();
		liste_vignettes.pop();
	}
	conteneur_bd.style.height = 'auto';
	const hauteur = conteneur_bd.getBoundingClientRect().height;
	conteneur_bd.style.height = hauteur + 'px';
	if (liste_bandes.length===1){bouton_moins.style.display=null;}
	sauvegarde();
}

// Fonction pour convertir les valeurs en pixels
function convertToPixels (value) {
	// Convertit la valeur en pixels en utilisant parseFloat
	return parseFloat(value);
};
  
function extractNumericValue(value) {
	// Utilisation d'une expression régulière pour extraire les nombres d'une chaîne
	var numericValue = parseFloat(value);
	return isNaN(numericValue) ? 0 : numericValue;
}

function verifieBackground(vignette){
	if (getbackgroundSizeActuel(vignette)<100){
		vignette.firstChild.style.backgroundSize='cover';
	}

	if (getbackgroundSizeActuel(vignette)<=100/getRatio(vignette)){
		vignette.firstChild.style.backgroundSize='cover';
	}
}

function opacite(objet,valeur){
	// Sélectionne tous les éléments ayant la classe "handle"
	const elements = document.querySelectorAll('.handle');
	// Parcourt chaque élément et affiche ses enfants
	elements.forEach(element => {
		element.style.opacity=valeur;
	});
}

function verifieVignette() {
    // Obtient les coordonnées de la souris
    const x = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;;
    const y = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
  
    // Récupère l'élément sous la position actuelle de la souris
    const elementsSousSouris = document.elementsFromPoint(x, y);
  
    // Filtrer la première div avec la classe "vignette"
    let premiereDivVignette = null;
    for (let i = 0; i < elementsSousSouris.length; i++) {
        if (elementsSousSouris[i].classList.contains('vignette')) {
            premiereDivVignette = elementsSousSouris[i];
            break;
        }
    }
    return premiereDivVignette;
}

function verifie_magnetisme(cartouche){
    let styles=window.getComputedStyle(cartouche);
    let leftCartouche=parseFloat(styles.left);
    let rightCartouche=parseFloat(styles.right);
    let topCartouche=parseFloat(styles.top);
    let bottomCartouche=parseFloat(styles.bottom);
  
    let position=[];
    
    if (leftCartouche<=20){position.push('cartouche_left')}
    else if (rightCartouche<=20){position.push('cartouche_right')}
    if (topCartouche<=20){position.push('cartouche_top')}
    else if (bottomCartouche<=20){position.push('cartouche_bottom')}
  
    return position;
}

function clique_input_couleur(input_couleur){
    input_couleur.click();
}

function showHandles(objet,valeur) {
    const enfantsAvecClasseHandle = objet.getElementsByClassName('handle');
	const enfantsArray = Array.from(enfantsAvecClasseHandle);
	enfantsArray.forEach(function(enfant) {
		enfant.style.display=valeur;
	});
}

function supprime_fond(vignette){
    let largeur_fond=getbackgroundSizeActuel(vignette)/100;
    vignette.firstChild.style.transition='background-position 0.8s linear';  
    vignette.firstChild.style.backgroundPosition='-'+largeur_fond*1024+'px'+' -'+largeur_fond*1024+'px';
	if (vignette.firstChild.getAttribute('fichier') !== null && vignette.firstChild.getAttribute('fichier') !== '') {
		imagesASupprimer.push(vignette.firstChild.getAttribute('fichier'));
		vignette.firstChild.removeAttribute('fichier');
	}
    maj_credits(vignette,'suppr');
    setTimeout(function() {
		vignette.firstChild.style.transition=null;
		vignette.firstChild.style.backgroundImage=null;
		vignette.classList.remove('background');
		vignette.firstChild.style.backgroundSize=null;
		vignette.firstChild.style.backgroundPosition=null;
    }, 500);
    sauvegarde();
}
  
function supprime(mode){
	if (clicked){
		if (clicked.classList.contains('vignette')){
			supprime_vignette(clicked);
		} else {
			if (clicked.classList.contains('cartouche')){
				clicked.parentNode.classList.remove('contient_cartouche');
			}
			clicked.remove();
			maj_credits(clicked,'suppr');
		}
	} else if (paragraphe_focus && mode!='clavier') {
		paragraphe_focus.parentNode.remove();
		maj_credits(paragraphe_focus.parentNode,'suppr');
		paragraphe_focus.focus=null;
		paragraphe_focus=null;
	}
	maj_credits_police();
	sauvegarde();
}
  
function retourne(objet,sens){
	let objets_a_retourner = Array.from(objet.children);
	objets_a_retourner.push(objet);

	for (var i = 0; i < objets_a_retourner.length; i++) {
		let element=objets_a_retourner[i];
		element.classList.remove('inversexy');
		element.classList.toggle('inverse'+sens);
		if (element.classList.contains('inversex') && element.classList.contains('inversey')) {
			element.classList.add('inversexy');
		}
	};
}

function miroir(sens){
	if (clicked || paragraphe_focus){
		let objet_a_modifier;    
		if (clicked) {objet_a_modifier = clicked;}
		if (paragraphe_focus){objet_a_modifier = paragraphe_focus.parentNode;}

		let image = objet_a_modifier.querySelector("img");
		
		if (clicked && image && !clicked.classList.contains('vignette')){retourne(image,sens);}
		else if (clicked && clicked.classList.contains('vignette')){retourne(clicked.firstChild,sens);}       
		else {retourne(objet_a_modifier,sens);}

		sauvegarde();
	}
}

function pre_ancre(){
	if (clicked){
		let vignette=null;
	
		if (clicked.classList.contains('ancre')){
			vignette=clicked.parentNode;
		} else {
			vignette=getVignette(clicked);
		}
	
		if (vignette){
			vignette.style.borderColor='purple';
			vignette_temporaire=vignette;
		}
	}
}
  
function fin_pre_ancre(){
	if (vignette_temporaire){
		vignette_temporaire.style.borderColor=null;
		vignette_temporaire=null;
	}
}
  
function ancre(event,cible){
	let ok=false;
	if (event===null || typeof event === 'undefined' || !event.target){ok=true;}
	else if (event.target.classList.contains('draggable') || event.target.parentNode.classList.contains('draggable') || event.target.parentNode.parentNode.classList.contains('draggable')){
		ok=true;  
	}
	else if (cible && (cible.classList.contains('draggable') || cible.parentNode.classList.contains('draggable') || cible.parentNode.parentNode.classList.contains('draggable'))){
		ok=true;  
	}
	if (ok){
		console.log('clicked='+clicked)
		if (clicked && clicked.classList.contains('ancrable')){
			let vignette=null;

			if (clicked.classList.contains('ancre')){
				vignette=clicked.parentNode;
			} else {
				vignette=getVignette(clicked);
			}
			
			if (vignette){
				const vignetteRect = vignette.getBoundingClientRect();
				const clickedRect = clicked.getBoundingClientRect();
				const scrollBD=bd.scrollTop;
				if (clicked.classList.contains('ancre')){     
					clicked.style.left=(clickedRect.left - decalage_bd) / ratio + 'px';
					clicked.style.top=(clickedRect.top - decalage_bdy + scrollBD) / ratio + 'px';
					conteneur_bd.appendChild(clicked);
					clicked.classList.remove('ancre');
					bouton_ancrer.classList.remove('annuler');
					bouton_ancrer.title="Ancrer l'objet";
				} else if (clicked.classList.contains('objet_galerie')) {
					clicked.classList.remove('objet_galerie');
					vignette.appendChild(clicked);
					const posX=(clickedRect.left - decalage_bd) / ratio;
					const posY=(clickedRect.top - decalage_bdy + scrollBD) / ratio;
					if (ratio !== 1) {
						clicked.style.width=(clickedRect.width / ratio) / ratio + 'px';
						clicked.style.height=(clickedRect.height / ratio) / ratio + 'px';
						if (clicked.querySelector('.cadre')) {
							clicked.querySelector('.cadre').style.width=(clickedRect.width / ratio) / ratio + 'px';
							clicked.querySelector('.cadre').style.height=(clickedRect.height / ratio) / ratio + 'px';
						}
						if (clicked.querySelector('.cadre img')) {
							clicked.querySelector('.cadre img').style.width=(clickedRect.width / ratio) / ratio + 'px';
							clicked.querySelector('.cadre img').style.height=(clickedRect.height / ratio) / ratio + 'px';
						}
					}
					clicked.style.left=(((posX*ratio) - (vignetteRect.left*ratio) - (3 * ratio)) / ratio) / ratio + 'px';
					clicked.style.top=(((posY*ratio) - (vignetteRect.top*ratio) - (3 * ratio)) / ratio) / ratio + 'px';
					clicked.classList.add('ancre');
					bouton_ancrer.classList.add('annuler');
					bouton_ancrer.title="Dés-ancrer l'objet";
				} else {
					vignette.appendChild(clicked);
					clicked.style.left=(clickedRect.left - vignetteRect.left - (3 * ratio)) / ratio + 'px';
					clicked.style.top=(clickedRect.top - vignetteRect.top - (3 * ratio)) / ratio + 'px';
					clicked.classList.add('ancre');
					bouton_ancrer.classList.add('annuler');
					bouton_ancrer.title="Dés-ancrer l'objet";
				}
			}
		}
	}
}

function cree_image(event,mode,url){
	let input = event.target;
	let nouvelle_image = new Image();
	nouvelle_image.classList.add('img_drag');
	if (mode==='bouton'){
		if (input.files && input.files[0]) {
			if (input.files[0].size < 1024000) {
				const racine = window.location.href.split('?')[0].replace('editeur.html', '');
				const urlParams = new URLSearchParams(window.location.search);
				const id = urlParams.get('id');
				const formData = new FormData();
				formData.append('bd', id);
				formData.append('blob', input.files[0]);
				chargement.style.display = 'block';
				const xhr = new XMLHttpRequest();
				xhr.onload = function () {
					if (xhr.readyState === xhr.DONE && xhr.status === 200) {
						const donnees = xhr.responseText;
						if (donnees === 'erreur') {
							chargement.style.display = 'none';
							input.value = null;
							alert('Erreur de communication avec le serveur.');
						} else {
							nouvelle_image.src = './fichiers/' + id + '/' + donnees;
							chargement.style.display = 'none';
							input.value = null;
						}
					} else {
						chargement.style.display = 'none';
						input.value = null;
						alert('Erreur de communication avec le serveur.');
					}
				};
				xhr.open('POST', racine + 'inc/televerser_image.php', true);
				xhr.send(formData);
			} else {
				input.value = null;
				alert('La taille maximale autorisée est 1 Mo.')
			}
		}
	} else {
		nouvelle_image.src=url;
	}
  
    let scrollBD=bd.scrollTop;
  
    let nouvel_objet = document.createElement('div');     
    nouvel_objet.classList.add('draggable','ancrable','objet');
    
    if (mode==='drop'){
		nouvel_objet.style.top=event.clientY+scrollBD-decalage_bdy-50+'px';
		nouvel_objet.style.left=event.clientX-decalage_bd-50+'px';
    } else {
		nouvel_objet.style.top=100+scrollBD+'px';
		nouvel_objet.style.left=100+'px';
    }
    let nouveau_cadre = document.createElement('div');
    nouveau_cadre.classList.add('cadre');
    
    // Attendre que l'image soit chargée
    nouvelle_image.onload = function() {
		let ratio_image=nouvelle_image.width/nouvelle_image.height;
		if (ratio_image<1){
			nouvelle_image.style.height=300+'px';
			nouvelle_image.style.width=300*ratio_image+'px';
		} else {
			nouvelle_image.style.width=300+'px';
			nouvelle_image.style.height=300/ratio_image+'px';
		}
		nouvel_objet.id=0;
		nouvelle_image.style.position='absolute';
		nouvelle_image.style.top=nouvelle_image.style.left='0px';
		nouvelle_image.id=0;
		nouvel_objet.classList.add('dragged');
		nouvel_objet.style.zIndex=hauteur;
		hauteur+=1;
		corry=10;
		corrx=10;
		clicked=nouvel_objet;
		bouton_ancrer.disabled=false;
		
		nouvel_objet.style.width=nouveau_cadre.style.width=nouvelle_image.style.width;
		nouvel_objet.style.height=nouveau_cadre.style.height=nouvelle_image.style.height;
		nouveau_cadre.classList.add('cadre');

		nouvel_objet.appendChild(nouveau_cadre);
		nouveau_cadre.appendChild(nouvelle_image);
		conteneur_bd.appendChild(nouvel_objet);

		createHandles(nouvel_objet);    
    };
    clicked=nouvel_objet;
    return nouvel_objet;
}

function cree_arriere_plan(event){
	let input = event.target;
	if (input.files && input.files[0] && cibleFond !== null) {
		if (input.files[0].size < 1024000) {
			const racine = window.location.href.split('?')[0].replace('editeur.html', '');
			const urlParams = new URLSearchParams(window.location.search);
			const id = urlParams.get('id');
			const formData = new FormData();
			formData.append('bd', id);
			formData.append('blob', input.files[0]);
			chargement.style.display = 'block';
			const xhr = new XMLHttpRequest();
			xhr.onload = function () {
				if (xhr.readyState === xhr.DONE && xhr.status === 200) {
					const donnees = xhr.responseText;
					if (donnees === 'erreur') {
						chargement.style.display = 'none';
						input.value = null;
						alert('Erreur de communication avec le serveur.');
					} else {
						chargement.style.display = 'none';
						const img = document.createElement('img');
						const objectURL = URL.createObjectURL(input.files[0]);
						img.onload = function () {
							cibleFond.firstChild.setAttribute('fichier', donnees);
							cibleFond.firstChild.style.backgroundImage='url(./fichiers/' + id + '/' + donnees + ')';
							cibleFond.firstChild.style.backgroundSize=(img.width / img.height) * 100 + '%';
							cibleFond.style.cursor = 'move';
							clicked = cibleFond;
							clicked.style.borderColor = 'red';
							clicked.classList.add('background');
							clicked.classList.add('vignette_selectionnee');
							URL.revokeObjectURL(objectURL);
							sauvegarde();
						};
						img.src = objectURL;
						input.value = null;
					}
				} else {
					chargement.style.display = 'none';
					input.value = null;
					alert('Erreur de communication avec le serveur.');
				}
			};
			xhr.open('POST', racine + 'inc/televerser_image.php', true);
			xhr.send(formData);
		} else {
			input.value = null;
			alert('La taille maximale autorisée est 1 Mo.')
		}
	}
}
  
function getVignette(clicked) {
    // Position de la souris
    let mouseX = event?.targetTouches?.[0]?.clientX || event.clientX;
    let mouseY = event?.targetTouches?.[0]?.clientY || event.clientY;
  
    // Récupérer la position absolue de l'élément cliqué
    const clickedRect = clicked.getBoundingClientRect();
  
    // Récupérer toutes les vignettes avec la classe 'vignette'
    const vignettes = document.querySelectorAll('.vignette');
  
    // Parcourir les vignettes pour trouver celle sous la position absolue de 'clicked'
    let vignetteSousClicked = null;
    let intersectionArea = 0;
    vignettes.forEach((vignette) => {
		const vignetteRect = vignette.getBoundingClientRect();

		// Calcul de l'intersection
		const intersectionWidth = Math.max(0, Math.min(clickedRect.right, vignetteRect.right) - Math.max(clickedRect.left, vignetteRect.left));
		const intersectionHeight = Math.max(0, Math.min(clickedRect.bottom, vignetteRect.bottom) - Math.max(clickedRect.top, vignetteRect.top));

		// Calcul de la surface partagée
		const newIntersectionArea = intersectionWidth * intersectionHeight;
		if (newIntersectionArea>intersectionArea){
			intersectionArea = newIntersectionArea;
			vignetteSousClicked = vignette;
		}
    });
  
    vignettes.forEach((vignette) => {
		const vignetteRect = vignette.getBoundingClientRect();
	
		// Vérifier si la vignette est en dessous de la souris
		if (
			mouseY>=vignetteRect.top &&
			mouseY<=vignetteRect.bottom &&
			mouseX>=vignetteRect.left &&
			mouseX<=vignetteRect.right
		) {
			vignetteSousClicked = vignette;
		}
    });
    return vignetteSousClicked;
}
  
function back(){
	if (clicked){
		const objets = bd.querySelectorAll('.draggable');
		objets.forEach(objet => {
			let position = parseInt(objet.style.zIndex,10);
			objet.style.zIndex=position+1;
		});
		hauteur+=1;
		clicked.style.zIndex=0;
		sauvegarde();
	}
}
  
function front(){
    if (clicked){
		clicked.style.zIndex=hauteur;
		hauteur+=1;
		sauvegarde();
    }
}
  
function police(facteur,taille){
  
	if (taille){police_size=taille;}
	else if (facteur !=0){police_size+=facteur;}

	let paragraphe=null;

	if (paragraphe_focus){
		paragraphe=paragraphe_focus;
	}

	else if (clicked){
		if (clicked.classList.contains('bubble') || clicked.classList.contains('onomatopee')){    
			paragraphe=clicked.querySelector('p');    
		}
	}

	if (paragraphe){
		let style_paragraphe=window.getComputedStyle(paragraphe);
		let fontSize = style_paragraphe.getPropertyValue('font-size');
		let currentFontSize = parseFloat(fontSize);
		if (!taille){police_size=currentFontSize+facteur;}
		paragraphe.style.fontSize=police_size+'px';    
		sauvegarde();
	}

	police_taille.value=police_size;
}

function change_police_couleur(couleur) {
	let paragraphe=null;

	if (paragraphe_focus){
		paragraphe=paragraphe_focus;
	}

	else if (clicked){
		if (clicked.classList.contains('bubble') || clicked.classList.contains('onomatopee')){    
			paragraphe=clicked.querySelector('p');    
		}
	}

	if (paragraphe){
		paragraphe.style.color=couleur;    
		sauvegarde();
	}

	police_couleur.value=rgbToHex(couleur);
}
  
function change_fond_couleur(couleur) {
	let zone;
	let pointe;
	let chemin_pointe;
	let paragraphe;

	if (paragraphe_focus){
		zone=paragraphe_focus.parentNode;
	} else if (clicked){
		if (clicked.classList.contains('bubble') || clicked.classList.contains('onomatopee') || clicked.classList.contains('cartouche')){    
			zone=clicked;
			paragraphe=zone.querySelector('p');
		}
	}

	if (zone){
		if (zone.classList.contains('bubble')){
			pointe = zone.querySelector('.pointe').querySelector('svg');
			chemin_pointe=pointe.getElementById('pointe');
		}

		if (zone.classList.contains('onomatopee')){
			paragraphe.style.WebkitTextStrokeColor=couleur;
		} else if (zone.classList.contains('cartouche')){ 
			zone.style.backgroundColor=couleur;
		} else if (zone.classList.contains('bubble')){ 
			zone.style.backgroundColor = couleur;  
			chemin_pointe.style.fill = couleur;
		}

		sauvegarde();
		fond_couleur.value=rgbToHex(couleur);
	}  
}
  
function deselectionnerTexte() {
    if (window.getSelection) {
        window.getSelection().removeAllRanges();
    } else if (document.selection) {
        // Pour les anciennes versions d'Internet Explorer
        document.selection.empty();
    }
}

function change_police(valeur){
	police_family=valeur;
	police_choix.style.fontFamily=valeur;
	let paragraphe=null;

	if (paragraphe_focus){
		paragraphe=paragraphe_focus;
	}

	else if (clicked){
		if (clicked.classList.contains('bubble')){    
			paragraphe=clicked.querySelector('p');    
		}
	}

	if (paragraphe){
		paragraphe.style.fontFamily=police_family;
		maj_credits_police();
		sauvegarde();
	}
}
  
// Déplacement fin au clavier
function deplace(x,y){
	if (clicked && clicked.classList.contains('bulle') || clicked.classList.contains('objet')){
		
		let oldTop = clicked.offsetTop;
		let oldLeft = clicked.offsetLeft;
	
		clicked.style.top = oldTop + y + 'px';
		clicked.style.left = oldLeft + x + 'px';
	}
}

function parametresUrl(key, value) {
	// Obtenez l'URL actuelle
	var currentUrl = window.location.href;

	// Utilisez URLSearchParams pour manipuler les paramètres de requête
	var urlParams = new URLSearchParams(window.location.search);

	// Vérifiez si le paramètre existe déjà
	if (urlParams.has(key)) {
		// Remplacez la valeur existante
		urlParams.set(key, value);
	} else {
		// Ajoutez le nouveau paramètre
		urlParams.append(key, value);
	}

	// Mettez à jour l'URL dans la barre d'adresse
	var newUrl = currentUrl.split('?')[0] + '?' + urlParams.toString();
	window.history.replaceState({}, document.title, newUrl);
}

function affiche_credits(zone) {
    if (zone){zone.classList.toggle('hide');}
    else {zone_credits.classList.toggle('hide');}
    sauvegarde();
}
  
function compterOccurrences(valeur, tableau) {
    var compteur = 0;
  
    for (var i = 0; i < tableau.length; i++) {
        if (tableau[i] === valeur) {
            compteur++;
        }
    }
  
    return compteur;
}
