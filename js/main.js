let admin = false;
let digidrive = false;
let titre = '';
let enregistrement = '';
let defilement = false;
let depart = 0;
let distance = 0;

// Pour requêtes xhr 
const racine = window.location.href.split('?')[0].replace('editeur.html', '');

// Nouveaux éléments DOM
const chargement = document.querySelector('#conteneur-chargement');
const bouton_debloquer = document.querySelector('#bouton_debloquer');
const bouton_parametres = document.querySelector('#bouton_parametres');
const bouton_enregistrer = document.querySelector('#bouton_enregistrer');
const barre_objets = document.querySelector('#barre_objets');
const conteneur_modale_connexion = document.querySelector('#conteneur-modale-connexion');
const conteneur_modale_parametres = document.querySelector('#conteneur-modale-parametres');
const modale_parametres = document.querySelector('#modale-parametres');
const modale_acces = document.querySelector('#modale-acces');
const modale_importer = document.querySelector('#modale-importer');
const modale_confirmation = document.querySelector('#modale-confirmation');
const input_question = document.querySelector('#question');
const input_reponse = document.querySelector('#reponse');
const input_question_actuelle = document.querySelector('#question-actuelle');
const input_reponse_actuelle = document.querySelector('#reponse-actuelle');
const input_nouvelle_question = document.querySelector('#nouvelle-question');
const input_nouvelle_reponse = document.querySelector('#nouvelle-reponse');

// On vérifie si des paramètres sont dans l'URL
let url = window.location.search;
let urlParams = new URLSearchParams(url);
if (urlParams.get('personnage')) { personnages.choix=urlParams.get('personnage'); }
else { personnages.choix = tirerAuSort(personnages)[0]; }
if (urlParams.get('fond')) { fonds.choix=urlParams.get('fond'); }
else { fonds.choix = tirerAuSort(fonds)[0]; }
if (urlParams.get('objet')) { objets.choix = urlParams.get('objet'); }
else { objets.choix=tirerAuSort(objets)[0]; }
if (urlParams.get('type_galerie')) {
	type_galerie_url = urlParams.get('type_galerie');
	if (type_galerie_url === 'personnages') { type_galerie=personnages;bouton_actif = bouton_personnages; }
	else if (type_galerie_url === 'fonds') { type_galerie=fonds;bouton_actif = bouton_fonds; }
	else if (type_galerie_url === 'objets') { type_galerie=objets;bouton_actif = bouton_objets; }
	else { type_galerie=personnages; }
} else { type_galerie=personnages; bouton_actif = bouton_personnages; }
if (urlParams.get('primtuxmenu') === 'true') {
	barre_outils.style.paddingLeft = '8rem';
}

// On bascule sur l'onglet fonds, personnages ou objets
change_galerie(type_galerie,bouton_actif, true);

// On démarre l'application avec une bande de 3 vignettes et on sauvegarde cet état.
cree_bande(3, true);
contenu_nouveau = bd.innerHTML;

largeur_bande_chargement = document.querySelector('#bd').getBoundingClientRect().width - 10;

let id = '';
if (urlParams.get('id') && urlParams.get('id') !== '') {
	id = urlParams.get('id');
}
if (id !== '') {
	// Vérification Digidrive
	const question = urlParams.get('q');
	const reponse = urlParams.get('r');
	if (question && question !== '' && reponse && reponse !== '') {
		const xhreq = new XMLHttpRequest()
		xhreq.onload = function () {
			if (xhreq.readyState === xhreq.DONE && xhreq.status === 200 && xhreq.responseText === 'bd_debloquee') {
				admin = true;
				digidrive = true;
			}
			let url = window.location.href.split('?')[0];
			url = url + '?id=' + id;
			window.history.replaceState({}, document.title, url);
		};
		xhreq.open('POST', racine + 'inc/ouvrir_bd.php', true);
		xhreq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhreq.send('id=' + id + '&question=' + window.atob(question) + '&reponse=' + window.atob(reponse) + '&type=api');
	}
	// Chargement du contenu enregistré
	const xhr = new XMLHttpRequest();
	xhr.onload = function () {
		if (xhr.readyState === xhr.DONE && xhr.status === 200) {
			chargement.style.display = 'none';
			if (xhr.responseText === 'contenu_inexistant' || verifierJSON(xhr.responseText) === false) {
				window.location.href = './';
				return false;
			}
			const reponse = JSON.parse(xhr.responseText)
			if (!reponse.titre || reponse.titre === '') {
				window.location.href = './';
				return false;
			}
			admin = reponse.admin;
			digidrive = Boolean(reponse.digidrive);
			titre = reponse.titre;
			document.title = titre + ' - Digistrip by La Digitale';
			if (admin === true) {
				bouton_debloquer.style.display = 'none';
				bouton_parametres.style.display = 'inline-block';
				enregistrement = setInterval(function () {
					enregistrerAutomatiquement(id, admin);
				}, 20000);
			}
			if (digidrive === true && document.querySelector('#afficher-supprimer')) {
				document.querySelector('#afficher-supprimer').style.display = 'none';
			}
			if (reponse.donnees !== '' && verifierJSON(reponse.donnees) === true) {
				let sauvegardesJson = reponse.donnees;
				liste_sauvegarde_cache = JSON.parse(sauvegardesJson);
				// Fix contenus avant v0.2.0
				if (liste_sauvegarde_cache.length > 4) {
					if (!liste_sauvegarde_cache[0].includes('conteneur_bd')) {
						bd.innerHTML = '';
						const div = document.createElement('div');
						div.innerHTML = '<main></main>';
						div.querySelector('main').innerHTML = liste_sauvegarde_cache[0];
						const divs = div.querySelectorAll('main > div');
						const titreS = div.querySelector('#titre_bd');
						const boutonsS = div.querySelector('#boutons');
						const creditsS = div.querySelector('#credits');
						const elements = [];
						divs.forEach(function (el) {
							if (el.id !== 'boutons' && el.id !== 'credits') {
								elements.push(el)
							}
						});
						elements.forEach(function (el) {
							if (el.style.transform) {
								el.style.transform = null;
							}
							if (el.style.transform) {
								el.style.transform = null;
							}
							if (el.style.marginBottom) {
								el.style.marginBottom = null;
							}
						});
						const conteneur = document.createElement('div');
						conteneur.id = 'conteneur_bd';
						elements.forEach(function (el) {
							conteneur.append(el);
						});
						bd.append(titreS);
						bd.append(conteneur);
						bd.append(boutonsS);
						bd.append(creditsS);
					} else {
						bd.innerHTML = liste_sauvegarde_cache[0];
					}
					liste_credits = liste_sauvegarde_cache[1].slice();
					liste_credits_pour_paragraphe = liste_sauvegarde_cache[2].slice();
					liste_credits_polices_pour_paragraphe = liste_sauvegarde_cache[3].slice();
					if (liste_sauvegarde_cache[4] === 0) {
						largeur_bande_bd_chargee = largeur_bande_chargement;
					} else {
						largeur_bande_bd_chargee = liste_sauvegarde_cache[4];
					}
					creeElementsDom();
					restaure_listes();    
					raz_globales();  
					update_boutons();
					maj_credits();
					redimensionner();
					if (liste_bandes.length < 2) {
						bouton_moins.style.display = null;
					} else {
						bouton_moins.style.display = 'inline-flex';
						bouton_moins.disabled = false;
					}
					bouton_plus.style.display = 'inline-flex';
					bouton_plus.disabled = false;
					largeur_bande = largeur_bande_bd_chargee;
					if (admin === false) {
						desactiver_edition();
					}
					sauvegarde();
				}
			} else {
				largeur_bande = largeur_bande_chargement;
				largeur_bande_bd_chargee = largeur_bande_chargement;
				if (admin === false) {
					desactiver_edition();
				}
				sauvegarde();
			}
			if (titre !== '') {
				document.querySelector('#titre_bd').textContent = titre;
			}
			activerDefilementHorizontal();
		} else {
			chargement.style.display = 'none';
			alert('Erreur de communication avec le serveur.');
		}
		chargement.classList.add('transparent');
	};
	xhr.open('POST', racine + 'inc/recuperer_bd.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.send('id=' + id);

	// Activation de l'événement d'enregistrement
	bouton_enregistrer.addEventListener('click', function () {
		enregistrer(id, admin);
	}, false)
} else {
	window.location.href = './';
}

input_question.value = '';
input_reponse.value = '';
input_question_actuelle.value = '';
input_reponse_actuelle.value = '';
input_nouvelle_question.value = '';
input_nouvelle_reponse.value = '';

// Afficher modale connexion pour accès admin
function voir_debloquer () {
	conteneur_modale_connexion.classList.add('ouvert');
	input_question.focus();
}

// Fermer modale connexion
function fermer_modale_connexion () {
	conteneur_modale_connexion.classList.remove('ouvert');
	input_question.value = '';
	input_reponse.value = '';
}

// Vérifier les informations pour débloquer la BD
function verifier_debloquer () {
	const question = input_question.value;
	const reponse = input_reponse.value;
	if (question !== '' && reponse !== '') {
		chargement.style.display = 'block';
		const xhr = new XMLHttpRequest();
		xhr.onload = function () {
			if (xhr.readyState === xhr.DONE && xhr.status === 200) {
				const donnees = xhr.responseText;
				if (donnees === 'erreur') {
					chargement.style.display = 'none';
					conteneur_modale_connexion.classList.remove('ouvert');
					alert('Erreur de communication avec le serveur.');
				} else if (donnees === 'contenu_inexistant') {
					window.location.href = './';
				} else if (donnees === 'non_autorise') {
					chargement.style.display = 'none';
					conteneur_modale_connexion.classList.remove('ouvert');
					alert('Vous n\'êtes pas autorisé à effectuer cette action');
				} else if (donnees === 'bd_debloquee') {
					chargement.style.display = 'none';
					conteneur_modale_connexion.classList.remove('ouvert');
					admin = true;
					bouton_debloquer.style.display = 'none';
					bouton_parametres.style.display = 'inline-block';
					const div = document.createElement('div');
					div.id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substr(2);
					div.innerHTML = 'Bande dessinée débloquée.';
					div.classList.add('notification');
					document.body.appendChild(div);
					input_question.value = '';
					input_reponse.value = '';
					activer_edition();
					enregistrement = setInterval(function () {
						enregistrerAutomatiquement(id, admin);
					}, 20000);
					setTimeout(function () {
						div.parentNode.removeChild(div);
					}, 2000);
				}
			} else {
				chargement.style.display = 'none';
				conteneur_modale_connexion.classList.remove('ouvert');
				alert('Erreur de communication avec le serveur.');
			}
		};
		xhr.open('POST', racine + 'inc/ouvrir_bd.php', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.send('id=' + id + '&question=' + question + '&reponse=' + reponse + '&type=digistrip');
	} else if (question === '') {
		alert('Veuillez sélectionner une question secrète.');
	} else if (reponse === '') {
		alert('Veuillez remplir le champ « Réponse secrète ».');
	}
}

// Afficher les paramètres
function voir_parametres () {
	modale_parametres.style.display = 'block';
	modale_confirmation.style.display = 'none';
	conteneur_modale_parametres.classList.add('ouvert');
}

// Afficher modale modifier accès
function voir_modifier_acces () {
	modale_acces.style.display = 'block';
	modale_parametres.style.display = 'none';
	input_question_actuelle.focus();
}

// Modifier l'accès
function modifier_acces () {
	const question = input_question_actuelle.value;
	const reponse = input_reponse_actuelle.value;
	const nouvellequestion = input_nouvelle_question.value;
	const nouvellereponse = input_nouvelle_reponse.value;
	if (question !== '' && reponse !== '' && nouvellequestion !== '' && nouvellereponse !== '') {
		chargement.style.display = 'block';
		const xhr = new XMLHttpRequest();
		xhr.onload = function () {
			if (xhr.readyState === xhr.DONE && xhr.status === 200) {
				const donnees = xhr.responseText;
				chargement.style.display = 'none';
				fermer_modale_parametres();
				if (donnees === 'contenu_inexistant') {
					window.location.href = './';
				} else if (donnees === 'erreur') {
					alert('Erreur de communication avec le serveur.');
				} else if (donnees === 'non_autorise') {
					alert('Vous n\'êtes pas autorisé à effectuer cette action');
				} else if (donnees === 'acces_modifie') {
					const div = document.createElement('div');
					div.id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substr(2);
					div.innerHTML = 'Accès modifié.';
					div.classList.add('notification');
					document.body.appendChild(div);
					setTimeout(function () {
						div.parentNode.removeChild(div);
					}, 2000);
				}
			} else {
				chargement.style.display = 'none';
				fermer_modale_parametres();
				alert('Erreur de communication avec le serveur.');
			}
		}
		xhr.open('POST', racine + 'inc/modifier_acces_bd.php', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.send('bd=' + id + '&question=' + question + '&reponse=' + reponse + '&nouvellequestion=' + nouvellequestion + '&nouvellereponse=' + nouvellereponse)
	} else if (question === '') {
		alert('Veuillez sélectionner votre question secrète actuelle.');
	} else if (reponse === '') {
		alert('Veuillez indiquer votre réponse secrète actuelle.');
	} else if (nouvellequestion === '') {
		alert('Veuillez sélectionner une nouvelle question secrète.');
	} else if (nouvellereponse === '') {
		alert('Veuillez indiquer une nouvelle réponse secrète.');
	}
}

// Exporter paramètres en zip
function exporter_archive () {
	fermer_modale_parametres();
	chargement.style.display = 'block';
	const nom = titre_bd.innerText + '.zip';
	const zip = new JSZip();
	const regexp = /fichiers[^)''"]+\.(?:jpg|jpeg|gif|png)/g;
	let fichiers = liste_sauvegarde_cache[0].match(regexp);
	if (fichiers === null) {
		fichiers = [];
	}
	fichiers = fichiers.filter(function (el) {
		return el.includes(id);
	});
	fichiers.forEach(function (fichier, index) {
		fichiers[index] = fichier.split('/').pop();
	});
	const donnees = { id: id, fichiers: fichiers, sauvegarde: liste_sauvegarde_cache };
	const donneesFichiers = [];
	for (const fichier of fichiers) {
		const donneesFichier = new Promise(function (resolve) {
			const xhr = new XMLHttpRequest();
			xhr.onload = function () {
				if (xhr.readyState === xhr.DONE && xhr.status === 200) {
					resolve({ nom: fichier, binaire: this.response });
				} else {
					resolve({ nom: '', binaire: '' });
				}
			}
			xhr.onerror = function () {
				resolve({ nom: '', binaire: '' });
			}
			xhr.open('GET', racine + 'fichiers/' + id + '/' + fichier, true);
			xhr.responseType = 'arraybuffer';
			xhr.send();
		});
		donneesFichiers.push(donneesFichier);
	}
	Promise.all(donneesFichiers).then(function (resultat) {
		resultat.forEach(function (item) {
			if (item.nom !== '' && item.binaire !== '') {
				zip.folder('fichiers').file(item.nom, item.binaire, { binary: true });
			}
		});
		zip.file('donnees.json', JSON.stringify(donnees));
		zip.generateAsync({ type: 'blob' }).then(function (archive) {
			chargement.style.display = 'none';
			saveAs(archive, nom + '_' + new Date().getTime() + '.zip');
			const div = document.createElement('div');
			div.id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substr(2);
			div.innerHTML = 'Bande dessinée exportée.';
			div.classList.add('notification');
			document.body.appendChild(div);
			setTimeout(function () {
				div.parentNode.removeChild(div);
			}, 2000);
		});
	});
}

// Afficher modale importer
function voir_importer () {
	modale_importer.style.display = 'block';
	modale_parametres.style.display = 'none';
}

// Importer fichier zip
function importer_archive (event) {
	const fichier = event.target.files[0];
	if (fichier === null || fichier.length === 0) {
		document.querySelector('#importer').value = '';
		return false
	} else {
		fermer_modale_parametres();
		chargement.style.display = 'block';
		const donneesFichiers = [];
		const jszip = new JSZip();
		jszip.loadAsync(fichier).then(function (archive) {
			if (archive.files['donnees.json'] && archive.files['donnees.json'] !== '') {
				archive.files['donnees.json'].async('string').then(function (donnees) {
					donnees = JSON.parse(donnees);
					let html = donnees.sauvegarde[0];
					html = html.replaceAll('./fichiers/' + donnees.id, './fichiers/' + id);
					donnees.sauvegarde[0] = html;
					const fichiers = donnees.fichiers;
					new Promise(function (resolve) {
						const xhr = new XMLHttpRequest()
						xhr.onload = function () {
							resolve('dossier_vide');
						}
						xhr.onerror = function () {
							resolve('erreur_televersement');
						}
						xhr.open('POST', racine + 'inc/vider_dossier_bd.php', true);
						xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
						xhr.send('bd=' + id);
					})
					for (const item of fichiers) {
						const donneesFichier = new Promise(function (resolve) {
							if (archive.files['fichiers/' + item]) {
								archive.files['fichiers/' + item].async('blob').then(function (blob) {
									const formData = new FormData();
									formData.append('fichier', item);
									formData.append('bd', id);
									formData.append('blob', blob);
									const xhr = new XMLHttpRequest();
									xhr.onload = function () {
										if (xhr.readyState === xhr.DONE && xhr.status === 200) {
											resolve('fichier_televerse');
										} else {
											resolve('erreur_televersement');
										}
									}
									xhr.onerror = function () {
										resolve('erreur_televersement');
									}
									xhr.open('POST', racine + 'inc/televerser_fichier_import.php', true);
									xhr.send(formData);
								})
							} else {
								resolve('erreur_televersement');
							}
						})
						donneesFichiers.push(donneesFichier);
					}
					Promise.all(donneesFichiers).then(function () {
						const xhr = new XMLHttpRequest();
						xhr.onload = function () {
							if (xhr.readyState === xhr.DONE && xhr.status === 200) {
								if (xhr.responseText === 'erreur') {
									alert('Erreur de communication avec le serveur.');
								} else if (xhr.responseText === 'non_autorise') {
									alert('Vous n\'êtes pas autorisé à effectuer cette action.');
								} else if (xhr.responseText === 'bd_modifiee') {
									liste_sauvegarde_cache = donnees.sauvegarde;
									if (liste_sauvegarde_cache.length > 4) {
										bd.innerHTML = liste_sauvegarde_cache[0];
										liste_credits = liste_sauvegarde_cache[1].slice();
										liste_credits_pour_paragraphe = liste_sauvegarde_cache[2].slice();
										liste_credits_polices_pour_paragraphe = liste_sauvegarde_cache[3].slice();
										if (liste_sauvegarde_cache[4] === 0) {
											largeur_bande_bd_chargee = largeur_bande_chargement;
										} else {
											largeur_bande_bd_chargee = liste_sauvegarde_cache[4];
										}
										creeElementsDom();
										restaure_listes();    
										raz_globales();  
										update_boutons();
										maj_credits();
										redimensionner();
										if (liste_bandes.length < 2) {
											bouton_moins.style.display = null;
										} else {
											bouton_moins.style.display = 'inline-flex';
											bouton_moins.disabled = false;
										}
										bouton_plus.style.display = 'inline-flex';
										bouton_plus.disabled = false;
										largeur_bande = largeur_bande_bd_chargee;
										if (admin === false) {
											desactiver_edition();
										}
										sauvegarde();
										const div = document.createElement('div');
										div.id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substr(2);
										div.innerHTML = 'Bande dessinée importée.';
										if (enregistrement !== '') {
											clearInterval(enregistrement);
										}
										enregistrement = setInterval(function () {
											enregistrerAutomatiquement(id, admin);
										}, 20000)
										div.classList.add('notification');
										document.body.appendChild(div);
										setTimeout(function () {
											div.parentNode.removeChild(div);
										}, 2000);
									}
								}
								document.querySelector('#importer').value = '';
								chargement.style.display = 'none';
							} else {
								document.querySelector('#importer').value = '';
								chargement.style.display = 'none';
								alert('Erreur de communication avec le serveur.');
							}
						};
						xhr.open('POST', racine + 'inc/modifier_bd.php', true);
						xhr.setRequestHeader('Content-type', 'application/json');
						const json = { bd: id, donnees: JSON.stringify(donnees.sauvegarde) };
						xhr.send(JSON.stringify(json));
					})
				})
			}
		})
	}
}

// Fermer modale paramètres
function fermer_modale_parametres () {
	conteneur_modale_parametres.classList.remove('ouvert');
	modale_parametres.style.display = 'none';
	modale_importer.style.display = 'none';
	modale_acces.style.display = 'none';
	input_question_actuelle.value = '';
	input_reponse_actuelle.value = '';
	input_nouvelle_question.value = '';
	input_nouvelle_reponse.value = '';
}

// Afficher la confirmation de suppression de la BD
function voir_supprimer_bd () {
	modale_parametres.style.display = 'none';
	modale_confirmation.style.display = 'block';
}

// Supprimer la bande dessinée
function supprimer_bd () {
	chargement.style.display = 'block';
	conteneur_modale_parametres.classList.remove('ouvert');
	const xhr = new XMLHttpRequest();
	xhr.onload = function () {
		if (xhr.readyState === xhr.DONE && xhr.status === 200) {
			const donnees = xhr.responseText;
			if (donnees === 'erreur') {
				chargement.style.display = 'none';
				alert('Erreur de communication avec le serveur.');
			} else if (donnees === 'contenu_inexistant' || donnees === 'bd_supprimee') {
				window.location.href = './';
			} else if (donnees === 'non_autorise') {
				chargement.style.display = 'none';
				alert('Vous n\'êtes pas autorisé à effectuer cette action');
			}
		} else {
			chargement.style.display = 'none';
			alert('Erreur de communication avec le serveur.');
		}
	};
	xhr.open('POST', racine + 'inc/supprimer_bd.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.send('id=' + id);
}

// Terminer session d'édition de la bande dessinée
function terminer_edition () {
	chargement.style.display = 'block';
	conteneur_modale_parametres.classList.remove('ouvert');
	const xhr = new XMLHttpRequest();
	xhr.onload = function () {
		if (xhr.readyState === xhr.DONE && xhr.status === 200) {
			chargement.style.display = 'none';
			const donnees = xhr.responseText;
			if (donnees === 'session_terminee') {
				admin = false;
				bouton_debloquer.style.display = 'inline-block';
				bouton_parametres.style.display = 'none';
				desactiver_edition();
				if (enregistrement !== '') {
					clearInterval(enregistrement);
				}
			}
		} else {
			chargement.style.display = 'none';
			alert('Erreur de communication avec le serveur.');
		}
	};
	xhr.open('POST', racine + 'inc/terminer_session_bd.php', true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.send('id=' + id);
}

// Fonction appelée au redimensionnement de la fenêtre
window.addEventListener('resize', function () {
	largeur_bande_chargement = document.querySelector('#bd').getBoundingClientRect().width - 10;
	conteneur_bd.style.transform = 'none';
	conteneur_bd.style.height = 'auto';
	ratio = largeur_bande_chargement / largeur_bande_bd_chargee;
	if (largeur_bande_bd_chargee !== largeur_bande_chargement) {
		conteneur_bd.style.width = 'auto';
		conteneur_bd.style.transform = 'scale(' + ratio + ')';
		conteneur_bd.style.transformOrigin = 'top left';
		conteneur_bd.style.width = largeur_bande_chargement / ratio + 'px';
	}
	const bandes = document.querySelectorAll('.bande');
	bandes.forEach(function (bande, index) {
		if (index > 0) {
			bande.style.marginTop = '5px';
		} else {
			bande.style.marginTop = '0px';
		}
		const vignettes = bande.querySelectorAll('.vignette');
		vignettes.forEach(function (vignette) {
			vignette.querySelector('.outils_vignette.haut').style.transform = 'none';
			vignette.querySelector('.outils_vignette.bas').style.transform = 'none';
			vignette.querySelector('.outils_vignette.gauche').style.transform = 'none';
			if (largeur_bande_bd_chargee !== largeur_bande_chargement) {
				vignette.querySelector('.outils_vignette.haut').style.transform = 'scale(' + 1 / ratio + ')';
				vignette.querySelector('.outils_vignette.haut').style.transformOrigin = 'top center';
				vignette.querySelector('.outils_vignette.bas').style.transform = 'scale(' + 1 / ratio + ')';
				vignette.querySelector('.outils_vignette.bas').style.transformOrigin = 'bottom center';
				vignette.querySelector('.outils_vignette.gauche').style.transform = 'scale(' + 1 / ratio + ')';
				vignette.querySelector('.outils_vignette.gauche').style.transformOrigin = 'left center';
			}
		});
	});
	const hauteurConteneur = conteneur_bd.getBoundingClientRect().height;
	conteneur_bd.style.height = hauteurConteneur + 'px';
	galerie_images.style.height = (galerie.offsetHeight - choix.offsetHeight - zone_gros_boutons.offsetHeight - 30)+'px';
}, false);

// Enregistrement des données de la BD
function enregistrer (id, admin) {
	if (id !== '' && admin === true) {
		chargement.style.display = 'block';
		const xhr = new XMLHttpRequest();
		xhr.onload = function () {
			if (xhr.readyState === xhr.DONE && xhr.status === 200) {
				const donnees = xhr.responseText;
				if (donnees === 'erreur') {
					alert('Erreur de communication avec le serveur.');
				} else if (donnees === 'non_autorise') {
					alert('Vous n\'êtes pas autorisé à effectuer cette action.');
				} else if (donnees === 'bd_modifiee') {
					const div = document.createElement('div');
					div.id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substr(2);
					div.innerHTML = 'Bande dessinée enregistrée.';
					if (enregistrement !== '') {
						clearInterval(enregistrement);
					}
					enregistrement = setInterval(function () {
						enregistrerAutomatiquement(id, admin);
					}, 20000)
					div.classList.add('notification');
					document.body.appendChild(div);
					setTimeout(function () {
						div.parentNode.removeChild(div);
					}, 2000);
				}
				chargement.style.display = 'none';
			} else {
				chargement.style.display = 'none';
				alert('Erreur de communication avec le serveur.');
			}
		};
		xhr.open('POST', racine + 'inc/modifier_bd.php', true);
		xhr.setRequestHeader('Content-type', 'application/json');
		const json = { bd: id, donnees: JSON.stringify(liste_sauvegarde_cache) };
		xhr.send(JSON.stringify(json));
	}
}

// Enregistrement automatique toute les 20 secondes
function enregistrerAutomatiquement (id, admin) {
	if (id !== '' && admin === true) {
		const xhr = new XMLHttpRequest();
		xhr.onload = function () {
			if (xhr.readyState === xhr.DONE && xhr.status === 200 && xhr.responseText === 'bd_modifiee') {
				const div = document.createElement('div');
				div.id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substr(2);
				div.innerHTML = 'Bande dessinée enregistrée.';
				div.classList.add('notification');
				document.body.appendChild(div);
				setTimeout(function () {
					div.parentNode.removeChild(div);
				}, 2000);
			}
		};
		xhr.open('POST', racine + 'inc/modifier_bd.php', true);
		xhr.setRequestHeader('Content-type', 'application/json');
		const json = { bd: id, donnees: JSON.stringify(liste_sauvegarde_cache) };
		xhr.send(JSON.stringify(json));
	}
}

// Vérifier si données sont conformes
function verifierJSON (json) {
	try {
		JSON.parse(json);
		return true;
	} catch {
		return false;
	}
}

// Fonction appelée au chargement initial de la page pour vérifier le ratio et redimensionner les éléments
function redimensionner () {
	conteneur_bd.style.transform = 'none';
	conteneur_bd.style.height = 'auto';
	if (largeur_bande_bd_chargee !== largeur_bande_chargement) {
		conteneur_bd.style.width = 'auto';
		ratio = largeur_bande_chargement / largeur_bande_bd_chargee;
		conteneur_bd.style.transform = 'scale(' + ratio + ')';
		conteneur_bd.style.transformOrigin = 'top left';
		conteneur_bd.style.width = largeur_bande_chargement / ratio + 'px';
	}
	const bandes = document.querySelectorAll('.bande');
	bandes.forEach(function (bande, index) {
		if (index > 0) {
			bande.style.marginTop = '5px';
		} else {
			bande.style.marginTop = '0px';
		}
		const vignettes = bande.querySelectorAll('.vignette');
		vignettes.forEach(function (vignette) {
			vignette.querySelector('.outils_vignette.haut').style.transform = 'none';
			vignette.querySelector('.outils_vignette.bas').style.transform = 'none';
			vignette.querySelector('.outils_vignette.gauche').style.transform = 'none';
			if (largeur_bande_bd_chargee !== largeur_bande_chargement) {
				vignette.querySelector('.outils_vignette.haut').style.transform = 'scale(' + 1 / ratio + ')';
				vignette.querySelector('.outils_vignette.haut').style.transformOrigin = 'top center';
				vignette.querySelector('.outils_vignette.bas').style.transform = 'scale(' + 1 / ratio + ')';
				vignette.querySelector('.outils_vignette.bas').style.transformOrigin = 'bottom center';
				vignette.querySelector('.outils_vignette.gauche').style.transform = 'scale(' + 1 / ratio + ')';
				vignette.querySelector('.outils_vignette.gauche').style.transformOrigin = 'left center';
			}
		});
	});
	const hauteurConteneur = conteneur_bd.getBoundingClientRect().height;
	conteneur_bd.style.height = hauteurConteneur + 'px';
}

// Éléments à désactiver quand admin = false
function desactiver_edition () {
	//bouton_nouveau.disabled = true;
	//bouton_nouveau.style.display = 'none';
	bouton_enregistrer.disabled = true;
	bouton_enregistrer.style.display = 'none';
	bouton_exporter.disabled = true;
	bouton_exporter.style.display = 'none';
	bouton_annuler.style.display = 'none';
	bouton_refaire.style.display = 'none';
	bouton_cadenas.disabled = true;
	bouton_cadenas.style.display = 'none';
	bouton_titre.disabled = true;
	bouton_titre.style.display = 'none';
	barre_texte.style.display = 'none';
	bouton_bulle.disabled = true;
	bouton_pensee.disabled = true;
	bouton_ordi.disabled = true;
	bouton_telephone.disabled = true;
	bouton_onomatopee.disabled = true;
	police_choix.disabled = true;
	police_taille.disabled = true;
	police_moins.disabled = true;
	police_plus.disabled = true;
	police_couleur.disabled = true;
	fond_couleur.disabled = true;
	barre_objets.style.display = 'none';
	bouton_image.disabled = true;
	bouton_nouvel_arriere_plan.disabled = true;
	bouton_ancrer.disabled = true;
	bouton_miroir.disabled = true;
	bouton_miroir_y.disabled = true;
	bouton_back.disabled = true;
	bouton_front.disabled = true;
	bouton_dupliquer.disabled = true;
	bouton_supprimer.disabled = true;
	galerie.style.display = 'none';
	bd.style.display = 'block';
	bd.style.overflow = 'hidden';
	bd.style.height = 'auto';
	bd.classList.add('lecture');
	formulaire_credits.style.display = 'none';
	bouton_plus.disabled = true;
	bouton_plus.style.display = 'none';
	bouton_moins.disabled = true;
	bouton_moins.style.display = 'none';
	const handles = document.querySelectorAll('.handle');
	handles.forEach(function (handle) {
		handle.style.opacity = 0;
	});
	if (document.querySelector('.vignette.vignette_selectionnee')) {
		document.querySelector('.vignette.vignette_selectionnee').classList.remove('vignette_selectionnee');
	}
	document.removeEventListener('touchstart', clic);
	document.removeEventListener('touchmove', move);
	document.removeEventListener('touchend', release);
	document.removeEventListener('mousedown', clic);
	document.removeEventListener('mousemove', move);
	document.removeEventListener('mouseup', release);
	document.removeEventListener('keydown', gererClavier);
	bd.removeEventListener('drop', gererDrop);
	document.removeEventListener('dblclick', ancre);
	titre_bd.contentEditable = false;
	document.body.style.overflowY = 'scroll';
	document.body.style.overflowX = 'hidden';
}

// Éléments à activer quand admin = true
function activer_edition () {
	//bouton_nouveau.disabled = false;
	//bouton_nouveau.style.display = 'inline-block';
	bouton_enregistrer.disabled = false;
	bouton_enregistrer.style.display = 'inline-block';
	bouton_exporter.disabled = false;
	bouton_exporter.style.display = 'inline-block';
	bouton_annuler.style.display = 'inline-block';
	bouton_refaire.style.display = 'inline-block';
	bouton_cadenas.disabled = false;
	bouton_cadenas.style.display = 'inline-block';
	bouton_titre.disabled = false;
	bouton_titre.style.display = 'inline-block';
	barre_texte.style.display = 'inherit';
	bouton_bulle.disabled = false;
	bouton_pensee.disabled = false;
	bouton_ordi.disabled = false;
	bouton_telephone.disabled = false;
	bouton_onomatopee.disabled = false;
	police_choix.disabled = false;
	police_taille.disabled = false;
	police_moins.disabled = false;
	police_plus.disabled = false;
	police_couleur.disabled = false;
	fond_couleur.disabled = false;
	barre_objets.style.display = 'inherit';
	bouton_image.disabled = false;
	bouton_nouvel_arriere_plan.disabled = false;
	bouton_ancrer.disabled = false;
	bouton_miroir.disabled = false;
	bouton_miroir_y.disabled = false;
	bouton_back.disabled = false;
	bouton_front.disabled = false;
	bouton_dupliquer.disabled = false;
	bouton_supprimer.disabled = false;
	galerie.style.display = 'inline-block';
	galerie_images.style.height = (galerie.offsetHeight - choix.offsetHeight - zone_gros_boutons.offsetHeight - 30)+'px';
	bd.style.display = 'inline-block';
	bd.style.overflowY = 'auto';
	bd.style.overflowX = 'hidden';
	bd.style.height = 'calc(100% - 70px)';
	bd.classList.remove('lecture');
	formulaire_credits.style.display = 'block';
	bouton_plus.disabled = false;
	bouton_plus.style.display = 'inline-flex';
	bouton_moins.disabled = false;
	if (liste_bandes.length > 1) {
		bouton_moins.style.display = 'inline-flex';
	}
	document.addEventListener('touchstart', clic);
	document.addEventListener('touchmove', move);
	document.addEventListener('touchend', release);
	document.addEventListener('mousedown', clic);
	document.addEventListener('mousemove', move);
	document.addEventListener('mouseup', release);
	document.addEventListener('keydown', gererClavier, false);
	bd.addEventListener('drop', gererDrop);
	document.addEventListener('dblclick', ancre);
	titre_bd.contentEditable = true;
	document.body.style.overflow = 'hidden';
}

function activerDefilementHorizontal () {
	barre_outils.addEventListener('mousedown', defilementHorizontalDebut);
	barre_outils.addEventListener('mouseleave', defilementHorizontalFin);
	barre_outils.addEventListener('mouseup', defilementHorizontalFin);
	barre_outils.addEventListener('mousemove', defilementHorizontalEnCours);
}

function defilementHorizontalDebut (event) {
	defilement = true;
	depart = event.pageX - barre_outils.offsetLeft;
	distance = barre_outils.scrollLeft;
}

function defilementHorizontalFin () {
	defilement = false;
}

function defilementHorizontalEnCours (event) {
	if (!defilement) { return }
	event.preventDefault();
	const x = event.pageX - barre_outils.offsetLeft;
	const delta = (x - depart) * 1.5;
	barre_outils.scrollLeft = distance - delta;
}
