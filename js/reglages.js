
// Boutons crédits
bouton_credits.checked=true;
bouton_autres_credits.checked=false;

// Réglage des contrôles
police_taille.value = 24;
police_couleur.value = '#000000';
fond_couleur.value = '#ffffff';

bouton_annuler.disabled = true;
bouton_refaire.disabled = true;
bouton_ancrer.disabled = true;

// Tailles de polices
let tailles = [
	6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70];
// Ajout des tailles dans le menu déroulant
for (var i = 0; i < tailles.length; i++) {
	let option = document.createElement("option");
	option.text = tailles[i];
	option.classList.add('police');
	police_taille.add(option);
}

// Variables globales
let redim=false;
let dragged=null;
let clicked=null;
let paragraphe_focus=null;
let hauteur=1;
let bordureClic=5;
let police_family='FuntypeRegular';
let police_size=24;
let vignette_background=null;
let position=-1;
let corry=0;
let corrx=0;
let borderWidth = 16;
let lock=false;
let ew_resize = 'col-resize';
let ns_resize = 'row-resize';
let licence;
police_choix.value=police_family;
let reclic=false;
let vignette_temporaire=null;
let decalage_bd=bd.offsetLeft+5;
let decalage_bdy=bd.offsetTop+5;

// Listes
let sauvegardes = [];
let liste_credits = [];
let sauvegardes_credits = [];
let liste_credits_pour_paragraphe=[];
let sauvegardes_liste_credits_pour_paragraphe=[];
let liste_credits_polices_pour_paragraphe=[];
let sauvegardes_liste_credits_polices_pour_paragraphe=[];

let liste_sauvegarde_cache = [];

let liste_bandes = [];
let liste_vignettes = [];

// Pour cible fond avec bouton
let cibleFond = null;

// Listes des fichiers à supprimer lors d'une sauvegarde
let imagesASupprimer = [];

// Pour redimensionnement
let largeur_bande_chargement = 0;
let largeur_bande_bd_chargee = 0;
let largeur_bande = 0;
let ratio = 1;

// Détections
let posXbd = bd.offsetLeft;
let posYbd = bd.offsetTop;
