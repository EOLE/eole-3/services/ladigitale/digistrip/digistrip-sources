function sauvegarde(){
	// Supression éventuelle des sauvegardes inutiles
	if (position<sauvegardes.length-1) {
		sauvegardes.splice(position+1);
		sauvegardes_credits.splice(position+1);
		sauvegardes_liste_credits_pour_paragraphe.splice(position+1);
		sauvegardes_liste_credits_polices_pour_paragraphe.splice(position+1);
	}
  
    // Sauvegarde du contenu de la BD
    const sauvegarde = bd.innerHTML;
    const copie_liste_credits = liste_credits.slice();
    const copie_liste_credits_pour_paragraphe = liste_credits_pour_paragraphe.slice();
    const copie_liste_credits_polices_pour_paragraphe = liste_credits_polices_pour_paragraphe.slice();
  
    sauvegardes.push(sauvegarde);
    sauvegardes_credits.push(copie_liste_credits);
    sauvegardes_liste_credits_pour_paragraphe.push(copie_liste_credits_pour_paragraphe);
    sauvegardes_liste_credits_polices_pour_paragraphe.push(copie_liste_credits_polices_pour_paragraphe);
  
    // On avance d'une position
    position+=1;
  
    // On met à jour les boutons annuler / refaire
    update_boutons();

    // Préparer la sauvegarde
    liste_sauvegarde_cache = [sauvegarde,copie_liste_credits,copie_liste_credits_pour_paragraphe,copie_liste_credits_polices_pour_paragraphe,largeur_bande];
}
  
function annuler(){
	if (position>0){ // Empêche d'annuler en-deça de la position 0
		// On recule d'une position
		position-=1;
		// Restauration du contenu de la BD
		bd.innerHTML=sauvegardes[position];
		liste_credits=sauvegardes_credits[position].slice();
		liste_credits_pour_paragraphe=sauvegardes_liste_credits_pour_paragraphe[position].slice();
		liste_credits_polices_pour_paragraphe=sauvegardes_liste_credits_polices_pour_paragraphe[position].slice();

		// Reconstitution des listes de bandes et vignettes
		restaure_listes();    
		raz_globales();
		update_boutons();
	}
}
  
function refaire(){
	if (position<sauvegardes.length-1){ // Empêche de restaurer plus loin que la liste des positions
		// On avance d'une position
		position+=1;
		// Restauration du contenu de la BD
		bd.innerHTML=sauvegardes[position];
		liste_credits=sauvegardes_credits[position].slice();
		liste_credits_pour_paragraphe=sauvegardes_liste_credits_pour_paragraphe[position].slice();
		liste_credits_polices_pour_paragraphe=sauvegardes_liste_credits_polices_pour_paragraphe[position].slice();
		// Réattribution des éléments DOM
		creeElementsDom();
		// Reconstitution des listes de bandes et vignettes
		restaure_listes();
		raz_globales();
		update_boutons();
	}
}
  
function update_boutons(){
	if (position<1){bouton_annuler.disabled=true;}
	else {bouton_annuler.disabled=false;}

	if (position===sauvegardes.length-1){bouton_refaire.disabled=true;}
	else {bouton_refaire.disabled=false;}
}
  
function raz_globales(){
	// On récupère l'objet cliqué s'il y en a un
	let elements = document.querySelectorAll('.vignette, .objet');
	let objet_a_recuperer=null;
	elements.forEach(element => {
		if (element.classList.contains('clicked') || element.classList.contains('vignette_selectionnee')){objet_a_recuperer=element}
	});
	if (objet_a_recuperer){clicked=objet_a_recuperer;clicked.click=true;}

	// On récupère le focus sur le paragraphe s'il y en a un
	elements = document.querySelectorAll('p');
	let paragraphe_a_recuperer=null;
	elements.forEach(element => {
		if (element.focus){paragraphe_a_recuperer=element}
	});
	if (paragraphe_a_recuperer){paragraphe_focus=paragraphe_a_recuperer;paragraphe_focus.focus=true;}

	// On réatttibue les évènements onclick
	elements = document.querySelectorAll('.vignette');
	elements.forEach(element => {
		const tousLesBoutons = element.querySelectorAll('button','input');
		const input_couleur = element.querySelector('input');
		const liste_fonctions = [
			{ fonction: zoom, arguments: [element,1] },
			{ fonction: zoom, arguments: [element,-1] },
			{ fonction: supprime_fond, arguments: [element] },
			{ fonction: clique_input_couleur, arguments: [input_couleur] },
			{ fonction: ajoute_vignette, arguments:[element,0] },
			{ fonction: supprime_vignette, arguments: [element] },
			{ fonction: ajoute_vignette, arguments: [element,1] },
			{ fonction: cartouche, arguments:[element] },
		];
		
		// Attribution de la fonction à l'input
		input_couleur.addEventListener("input", function() {
			change_couleur(element,input_couleur.value);
		});
		// Attribution des fonctions aux boutons
		tousLesBoutons.forEach((button, index) => {
			button.addEventListener("click", function() {
			liste_fonctions[index].fonction.apply(null, liste_fonctions[index].arguments);
			});
		});
	});
}

function restaure_listes(){
	// Sélection de tous les éléments div avec la classe 'bande'
	let divsBande = document.querySelectorAll('div.bande');
	// Remise à zéro de la liste des bandes
	liste_bandes = [];
	// Copie des éléments dans la liste
	divsBande.forEach(function(div) {
		liste_bandes.push(div);
	});

		// Remise à zéro de la liste des vignettes
	liste_vignettes = [];
		// Parcours de la liste des bandes
		liste_bandes.forEach(function(bande) {
		// Sélection des div enfants avec la classe 'vignette' pour la bande actuelle
		let vignettes = bande.querySelectorAll('.vignette');    
		// Stockage des vignettes de la bande actuelle dans une liste temporaire
		let tableauVignettes = [];
		vignettes.forEach(function(vignette) {
			tableauVignettes.push(vignette);
		});    
		// Ajout du tableau de vignettes de la bande actuelle au tableau liste_vignettes
		liste_vignettes.push(tableauVignettes);
	});
}

function creerImage(){
	// Désactivation des éléments indésirables
	bouton_moins.style.display="none";
	bouton_plus.style.display="none";
	formulaire_credits.style.display="none";
	bd.style.height='auto';
	bd.style.borderRadius='0px';
	if (clicked){declique();}
	let hauteur=bd.offsetHeight;
	let largeur=bd.offsetWidth;
	domtoimage
	.toJpeg(bd, { quality: 0.95, width: largeur, height:hauteur})
	.then(function (dataUrl) {
		bouton_moins.style.display=null;
		bouton_plus.style.display=null;
		formulaire_credits.style.display=null;
		bd.style.height=null;
		bd.style.borderRadius=null;
		saveAs(dataUrl, titre_bd.innerText + '.jpeg');
	});
}
  
function enregistre() {
	var contenu = bd.innerHTML;

	var fichier = new Blob([contenu], { type: 'text/plain' });
	var lien = document.createElement('a');
	lien.href = URL.createObjectURL(fichier);

	// Définir un nom de fichier par défaut (peut être vide ici)
	lien.setAttribute('download', 'bd_sauvegardee.txt');

	lien.click();
}
  
function ouvre(){
	var input = document.getElementById('choisirFichier');

	input.addEventListener('change', function() {
		var file = input.files[0];
		var reader = new FileReader();

		reader.onload = function(event) {
		var contenu = event.target.result;
		bd.innerHTML = contenu;
		};

		reader.readAsText(file);
	});

	// Déclenche le sélecteur de fichier
	input.click();
}
