// Reclic sur un objet déjà cliqué
function reclique(objet){
	objet.classList.toggle('reclic');
}
  
// L'utilisateur a cliqué
function clic(event) {
	let cible = event.target;
	console.log("cible="+cible.classList);
	if (cibleFond !== null && cible.id !== 'bouton_nouvel_arriere_plan') {
		cibleFond=null;
	}
	if (cible.classList.contains('div_fond')) {
		cibleFond=cible.parentNode;
	}
	if (cible.classList.contains('img_bulle') || cible.classList.contains('div_fond') || cible.classList.contains('img_fond')) {
		cible=cible.parentNode;
	}
	if (cible.classList.contains('img_drag') ) {
		cible=cible.parentNode.parentNode;
	}
	posXbd=bd.offsetLeft;
	posYbd=bd.offsetTop;

	if (cible===clicked) {reclic=cible;}

	if (!reclic){declique('clic',cible);}

	posX = event?.targetTouches?.[0]?.clientX || event.clientX;
	posY = event?.targetTouches?.[0]?.clientY || event.clientY; 

	decalage_bd=bd.offsetLeft+5;
	decalage_bdy=bd.offsetTop+5;
	const scrollGalerie=galerie_images.scrollTop;
	const scrollBD=bd.scrollTop;
  
	if (cible.classList.contains('draggable')) {
		dragged = cible;
		clicked = cible;        
		cible.click=true;
		event.preventDefault();
		vignette_en_cours=dragged.parentNode;

		posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        diffsourisx = posX - posX_objet;
		diffsourisy = posY - posY_objet;

		dragged.classList.add('dragged');
		clicked.classList.add('clicked');
		//cible.style.zIndex=hauteur;
		//hauteur+=1;
		showHandles(clicked,null);
		if (dragged.classList.contains('cartouche')){
			let styles=window.getComputedStyle(dragged);
			let leftCartouche=styles.left;
			let topCartouche=styles.top;
			dragged.style.top=topCartouche;
			dragged.style.left=leftCartouche;
			dragged.classList.remove('cartouche_top');
			dragged.classList.remove('cartouche_bottom');
			dragged.classList.remove('cartouche_left');
			dragged.classList.remove('cartouche_right');
		}
		if (dragged.querySelector('p')){maj_controles(dragged);}   
	} else if (cible.classList.contains('personnage')) {
        event.preventDefault();
        diffsourisx = (posX - cible.offsetLeft);
        diffsourisy = (posY - cible.offsetTop + scrollGalerie);
        posX_objet = cible.offsetLeft;
        posY_objet = cible.offsetTop;

        let nouvel_objet = document.createElement('div');     
        nouvel_objet.classList.add('objet');
        nouvel_objet.classList.add('draggable','ancrable');
        nouvel_objet.classList.add('temporaire','clicked');
        nouvel_objet.style.top=posY_objet-posYbd-scrollGalerie+scrollBD+'px';
        nouvel_objet.style.left=posX_objet-posXbd+'px';
        nouvel_objet.style.width=cible.offsetWidth+'px';
        nouvel_objet.style.height=cible.offsetHeight+'px';    
        nouvel_objet.id=0;  
        galerie.appendChild(nouvel_objet);
        bouton_ancrer.disabled=false;
  
        let nouveau_cadre = document.createElement('div');
        nouveau_cadre.classList.add('cadre');
        nouveau_cadre.style.width=nouvel_objet.style.width;
        nouveau_cadre.style.height=nouvel_objet.style.height;
  
        nouvel_objet.appendChild(nouveau_cadre);
        
        let nouvelle_image = document.createElement('img');     
        nouvelle_image.src=cible.src;
        nouvelle_image.classList.add('img_drag');
        nouvelle_image.style.width=cible.offsetWidth+'px';
        nouvelle_image.style.top=nouvelle_image.style.left='0px';
        nouvelle_image.style.position='absolute';
        nouvelle_image.id=0;
        nouveau_cadre.appendChild(nouvelle_image);
        nouvel_objet.classList.add('dragged');
        nouvel_objet.style.zIndex=hauteur;
        hauteur+=1;
        dragged=nouvel_objet;
        corry=0;
        corrx=0;
        clicked=nouvel_objet;
        createHandles(dragged);
        nouvel_objet.classList.add(cible.themecredit);
        nouvel_objet.themecredit=cible.themecredit;
	} else if (cible.classList.contains('fond') && cible.classList.contains('apercu')) {
        event.preventDefault();
        diffsourisx = (posX - cible.offsetLeft);
        diffsourisy = (posY - cible.offsetTop + scrollGalerie);
        posX_objet = cible.offsetLeft;
        posY_objet = cible.offsetTop;
        let nouvel_objet = document.createElement('div');     
        nouvel_objet.classList.add('objet');
        nouvel_objet.classList.add('fond');
        nouvel_objet.classList.add('draggable');
        nouvel_objet.classList.add('temporaire');
        nouvel_objet.style.top=posY_objet-posYbd-scrollGalerie+scrollBD+'px';
        nouvel_objet.style.left=posX_objet-posXbd+'px';
        nouvel_objet.style.width=cible.offsetWidth+'px';
        nouvel_objet.style.height=cible.offsetHeight+'px';
        nouvel_objet.source=cible.src;
        nouvel_objet.classList.add(cible.themecredit);
        nouvel_objet.themecredit=cible.themecredit;
  
        galerie.appendChild(nouvel_objet);
              
        let nouvelle_image = document.createElement('img');     
		nouvelle_image.src = cible.src;
        nouvelle_image.classList.add('img_fond');
        nouvelle_image.style.width=cible.offsetWidth+'px';
        nouvelle_image.style.height=cible.offsetHeight+'px';
  
        nouvel_objet.appendChild(nouvelle_image);
        nouvel_objet.classList.add('dragged');
        nouvel_objet.style.zIndex=hauteur;
        hauteur+=1;
        corry=0;
        corrx=0;
        dragged=nouvel_objet;
        clicked=nouvel_objet;
    } else if (cible.classList.contains('handle')) {
		let resizingHandle = cible;

		gauche=false;
		haut=false;

		if (resizingHandle.classList.contains('top')){
			haut=true;
		}
		if (resizingHandle.classList.contains('left')){
			gauche=true;
			haut=true;
		}

		let resizableElement = resizingHandle.parentNode;
		rogne=false;
		if (!cible.classList.contains('rogne')){
			resizableElement.classList.add('redim');
		}
		if (resizingHandle.classList.contains('rogne')){
			resizableElement = resizingHandle.parentNode.parentNode;
			resizableElement.classList.add('rognage');
			rogne=true;
		}

		cadre_a_redim = resizableElement.querySelector('.cadre');
		if (cadre_a_redim){cadre_a_redim.transition=null};
		if (cadre_a_redim && rogne){
			sauv_transform=cadre_a_redim.style.transform;
			//cadre_a_redim.style.transition='transform 0.2s linear';
			cadre_a_redim.style.transform='rotate(0deg)';
		}

		let image_a_redim;
		if (cadre_a_redim){image_a_redim = resizableElement.querySelector("img");}       
		let startX = event.clientX;
		let startY = event.clientY;
		let bulle = resizableElement.classList.contains('bubble') || resizableElement.classList.contains('bubble');
		let inversex = resizableElement.classList.contains('inversex');
		let inversey = resizableElement.classList.contains('inversey');
		let diagonale;
		let ratioCadre;

		// Récupération des valeurs de départ au moment du clic
		let startWidthResizable = parseFloat(document.defaultView.getComputedStyle(resizableElement).width, 10);
		let startHeightResizable = parseFloat(document.defaultView.getComputedStyle(resizableElement).height, 10);
		let startTopResizable = parseFloat(document.defaultView.getComputedStyle(resizableElement).top, 10)+decalage_bdy;
		let startLeftResizible = parseFloat(document.defaultView.getComputedStyle(resizableElement).left, 10)+decalage_bd;
		let pointe=null;
		// Calcul du centre de l'image
		let rectObjet=resizableElement.getBoundingClientRect();
		let centreX = rectObjet.left + rectObjet.width/2;
		let centreY = rectObjet.top + rectObjet.height/2;
		//   if (resizableElement.classList.contains('ancre')){ // si l'objet est ancré, on ajoute la position de la vignette associée.
		//     centreX = centreX + resizableElement.parentNode.offsetLeft;
		//     centreY = centreY + resizableElement.parentNode.offsetTop - scrollBD;
		//   }
		
		if (image_a_redim){
			startTopImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).top, 10);
			startLeftImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).left, 10);
			startWidthImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).width, 10);
			startHeightImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).height, 10);  
			startTopCadre = parseFloat(document.defaultView.getComputedStyle(cadre_a_redim).top, 10);
			startLeftCadre = parseFloat(document.defaultView.getComputedStyle(cadre_a_redim).left, 10);
			startWidthCadre = parseFloat(document.defaultView.getComputedStyle(cadre_a_redim).width, 10);
			startHeightCadre = parseFloat(document.defaultView.getComputedStyle(cadre_a_redim).height, 10);         
			objet_a_modifier =resizableElement.querySelector('.cadre');
			largeur_objet_a_modifier = startWidthCadre;
			left_objet_a_modifier = startLeftResizible;
			diagonale=Math.sqrt(startWidthCadre**2+startHeightCadre**2);
			ratioCadre = startWidthCadre/startHeightCadre;
			startAngleDegres=angleDeg(startX,startY,centreX,centreY);
		} else {
			objet_a_modifier = resizableElement;
			largeur_objet_a_modifier = startWidthResizable;
		}

		if (bulle){
			pointe = resizableElement.querySelector('.pointe');
			fleche = resizableElement.querySelector('.fleche');
			startLeftPointe = parseFloat(document.defaultView.getComputedStyle(pointe).left, 10);
			startLeftPoignee = parseFloat(document.defaultView.getComputedStyle(resizingHandle).left, 10);
			startBottomPoignee = parseFloat(document.defaultView.getComputedStyle(resizingHandle).bottom, 10);
			oldScaleY = parseFloat(pointe.id);
			let styles = window.getComputedStyle(resizableElement);
			let borderRadius = styles.borderRadius;
			let valeursBorderRadius = borderRadius.split(' ');
			let premiereValeurBorderRadius = valeursBorderRadius[0];
			var nombreSansUnite = parseInt(premiereValeurBorderRadius, 10);
			minLeftPointe = nombreSansUnite - 20;
			maxLeftPointe = startWidthResizable - (nombreSansUnite + 10);
			image_pointe = pointe.querySelector('svg');
			chemin_pointe=image_pointe.getElementById('pointe');
		}

		oldRotate=parseFloat(resizableElement.id)
		
		document.addEventListener('mousemove', resizeElement);
		document.addEventListener('mouseup', stopResize);
		document.addEventListener('touchstart', resizeElement);
		document.addEventListener('touchend', stopResize);
		
		// Début du redimensionnement
		function resizeElement(event) {
			// Position de la souris
			let mouseX = event?.targetTouches?.[0]?.clientX || event.clientX;
			let mouseY = event?.targetTouches?.[0]?.clientY || event.clientY;
		
			// Déplacement de la souris depuis la position de départ
			let diffX = mouseX - startX;
			let diffY = mouseY - startY;
			
			// Fonction de déplacement de la flèche de la bulle
			if (resizingHandle.classList.contains('fleche')) { 
				if (inversex){diffX*=-1;}
				if (inversey){diffY*=-1;}            
				let newLeftPointe = startLeftPointe + diffX;
				// Empêche de dépasser
				if (newLeftPointe<minLeftPointe){newLeftPointe=minLeftPointe} 
				else if (newLeftPointe>maxLeftPointe){newLeftPointe=maxLeftPointe}  

				pointe.style.left = newLeftPointe + 'px';
				resizingHandle.style.left = newLeftPointe + 'px';

				let newScaleY=oldScaleY+diffY/35;
				let newBottom=startBottomPoignee - diffY;
				if (newScaleY<0){newScaleY=0;newBottom=0;}
				if (resizableElement.classList.contains('pensee') && newScaleY<=8){
					pointe.style.height = 35*newScaleY + 'px';
					resizingHandle.style.bottom = newBottom + 'px';
					pointe.id=newScaleY;
				} else if (!resizableElement.classList.contains('pensee')){
					pointe.style.transform = 'scaleY('+newScaleY+')';
					resizingHandle.style.bottom = newBottom + 'px';
					pointe.id=newScaleY;
				}
			}

			// Rotation
			if (resizingHandle.classList.contains('rotate')) {
				if (inversex){diffX*=-1;}             
				newRotate = oldRotate + diffX/4;
				resizableElement.id=newRotate;
				if (newRotate<0){newRotate=360+newRotate;}              
				objet_a_modifier.style.transform = 'rotate('+newRotate+'deg)';              
				if (image_a_redim){
					let rotationRadian;
					if (newRotate<=90 || (newRotate>180 && newRotate<=270)){rotationRadian = (Math.PI/180) * newRotate;}
					else {rotationRadian = - (Math.PI/180) * newRotate;}                
					adapte_grand_cadre(rotationRadian,diagonale,ratioCadre,true); 
				}
			}
			// Fonctions "rogner"
			else if (resizingHandle.classList.contains('top')) { // Haut
			newHeight = startHeightCadre - diffY;
			if (newHeight<=startHeightImage){ // Empêche un rognage négatif
				resizableElement.style.height = startHeightResizable - diffY + 'px';
				resizableElement.style.top = startTopResizable + diffY - decalage_bdy + 'px';
				objet_a_modifier.style.height = newHeight + 'px';
				if (image_a_redim){
					image_a_redim.style.top=startTopImage-diffY+'px';}
				}
			} else if (resizingHandle.classList.contains('bottom')) { // Bas
				let newHeight = startHeightCadre + diffY;
				if (newHeight<=startHeightImage){ // Empêche un rognage négatif
					resizableElement.style.height = startHeightResizable + diffY + 'px';
					cadre_a_redim.style.height = newHeight + 'px';
				}            
			} else if (resizingHandle.classList.contains('left')) { // Gauche
				let newWidth = largeur_objet_a_modifier - diffX;
				if ((bulle && newWidth>=140) || (!bulle && newWidth<=startWidthImage)){ // Empêche un rognage négatif
					objet_a_modifier.style.width = newWidth + 'px';
					resizableElement.style.width = startWidthResizable - diffX + 'px';
					resizableElement.style.left = startLeftResizible + diffX - decalage_bd+ 'px';
					if (bulle){
					if (inversex){
						if (bulle && diffX >0 && startLeftPointe > newWidth - 45){
							let newLeftPointe = newWidth - 45;                  
							pointe.style.left = newLeftPointe + 'px';
							fleche.style.left = newLeftPointe + 'px';
						}
					} else {
						let newLeftPointe = startLeftPointe - diffX;
						if (newLeftPointe<minLeftPointe){newLeftPointe=minLeftPointe;}                  
						pointe.style.left = newLeftPointe + 'px';
						fleche.style.left = newLeftPointe + 'px';
					}                  
					}
					if (image_a_redim){image_a_redim.style.left=startLeftImage-diffX+'px';}
				}
			} else if (resizingHandle.classList.contains('right')) { // Droite
				let newWidth = largeur_objet_a_modifier + diffX;

				if ((bulle && newWidth>=140) || (!bulle && newWidth<=startWidthImage)) { // Empêche un rognage négatif
					let newWidth = largeur_objet_a_modifier + diffX;
					objet_a_modifier.style.width = newWidth + 'px';
					resizableElement.style.width = startWidthResizable + diffX + 'px';

					if (inversex){ // Si miroir
						let newLeftPointe = startLeftPointe + diffX;
						if (newLeftPointe<minLeftPointe){newLeftPointe=minLeftPointe;}                  
						pointe.style.left = newLeftPointe + 'px';
						fleche.style.left = newLeftPointe + 'px'; 

					} else { // Si pas miroir

						if (bulle && diffX <0 && startLeftPointe + 10 > newWidth - 35){
						let newLeftPointe = newWidth - 45;                  
						pointe.style.left = newLeftPointe + 'px';
						fleche.style.left = newLeftPointe + 'px';
						}
					}
				}
			// Fonctions "redimensionner"
			} else if(resizableElement.classList.contains('reclic')) {
				if (image_a_redim){
					cadre_a_redim.classList.add('rotation');
				}
				resizableElement.classList.add('rotation');

				if (image_a_redim){
					let newAngleDegres = angleDeg(mouseX,mouseY,centreX,centreY);
					newRotate = oldRotate + newAngleDegres - startAngleDegres;
					resizableElement.id=newRotate;
				}

				if (newRotate<0){newRotate=360+newRotate;}              
				objet_a_modifier.style.transform = 'rotate('+newRotate+'deg)';              
				if (image_a_redim){
					let rotationRadian;
					if (newRotate<=90 || (newRotate>180 && newRotate<=270)){rotationRadian = (Math.PI/180) * newRotate;}
					else {rotationRadian = - (Math.PI/180) * newRotate;}                
					adapte_grand_cadre(rotationRadian,diagonale,ratioCadre,true); 
				}
			}  else if (resizingHandle.classList.contains('top-left')) { // Haut gauche
				let diffXY = (diffX + diffY) / 4;
				let newWidth;
				newWidth = startWidthResizable - diffXY;
				resizableElement.style.width = newWidth + 'px';
				let newHeight = proportions(newWidth);
				let diffHeight = newHeight - startHeightResizable;
				resizableElement.style.left = startLeftResizible + diffXY - decalage_bd + 'px';
				resizableElement.style.top = startTopResizable - diffHeight - decalage_bdy + 'px';
			} else if (resizingHandle.classList.contains('top-right')) { // Haut droite
				let diffXY = (diffX - diffY) / 4;
				let newWidth;
				newWidth = startWidthResizable + diffXY;
				resizableElement.style.width = newWidth + 'px';             
				let newHeight=proportions(newWidth);
				let diffHeight = newHeight - startHeightResizable;
				resizableElement.style.top = startTopResizable - diffHeight - decalage_bdy + 'px';
			} else if (resizingHandle.classList.contains('bottom-left')) { // Bas gauche
				let diffXY = (diffX - diffY) / 4;
				let newWidth;
				newWidth = startWidthResizable - diffXY;
				resizableElement.style.width = newWidth + 'px';
				proportions(newWidth);
				resizableElement.style.left = startLeftResizible + diffXY - decalage_bd + 'px';
			} else if (resizingHandle.classList.contains('bottom-right')) { // bas droite
				let diffXY = (diffX + diffY) / 4;
				let newWidth;
				newWidth = startWidthResizable + diffXY;
				resizableElement.style.width = newWidth + 'px';
				proportions(newWidth);
			}

			function proportions(newWidth){   
				let agrandissement = newWidth / startWidthResizable;
				let newHeight = startHeightResizable * agrandissement;
				resizableElement.style.height = newHeight +'px';
				
				cadre_a_redim.style.width=startWidthCadre*agrandissement+'px';
				image_a_redim.style.width=startWidthImage*agrandissement+'px';
				cadre_a_redim.style.height=startHeightCadre*agrandissement+'px';
				image_a_redim.style.height=startHeightImage*agrandissement+'px';
				image_a_redim.style.top=startTopImage*agrandissement+'px';
				image_a_redim.style.left=startLeftImage*agrandissement+'px';
				return newHeight;
			}
		}
		
		function adapte_grand_cadre(rotationRadian,diagonale,ratioCadre,rotation,haut,gauche){              
			let nouvelle_hauteur = Math.abs(Math.cos(Math.atan(ratioCadre) - rotationRadian) * diagonale);
			let nouvelle_largeur = Math.abs(Math.cos(Math.atan(1/ratioCadre) - rotationRadian) * diagonale);              
			resizableElement.style.height=nouvelle_hauteur + 'px';
			resizableElement.style.width=nouvelle_largeur + 'px';
			if (rotation){
			resizableElement.style.left=startLeftResizible - (nouvelle_largeur-startWidthResizable) / 2 - decalage_bd+'px';
			resizableElement.style.top=startTopResizable - (nouvelle_hauteur-startHeightResizable) / 2 - decalage_bdy+'px';
			} else {
				if (gauche){
					resizableElement.style.left=startLeftResizible - (nouvelle_largeur-startWidthResizable) - decalage_bd+'px';
				}
				if (haut){
					resizableElement.style.top=startTopResizable - (nouvelle_hauteur-startHeightResizable) - decalage_bdy+'px';
				}
			}
		}           
	
		function stopResize() {
			if (cadre_a_redim && rogne){
				cadre_a_redim.style.transform=sauv_transform;           
				let rotationRadian;
				if (oldRotate<=90 || (oldRotate>180 && oldRotate<=270)){rotationRadian = (Math.PI/180) * oldRotate;}
				else {rotationRadian = - (Math.PI/180) * oldRotate;}            
				let nouvelle_largeur = parseFloat(document.defaultView.getComputedStyle(cadre_a_redim).width, 10);
				let nouvelle_hauteur = parseFloat(document.defaultView.getComputedStyle(cadre_a_redim).height, 10);
				let diagonale = Math.sqrt(nouvelle_largeur**2+nouvelle_hauteur**2);   
				let ratioCadre = nouvelle_largeur/nouvelle_hauteur;           
				adapte_grand_cadre(rotationRadian,diagonale,ratioCadre,false,haut,gauche);             
			}
			if (cadre_a_redim){
				cadre_a_redim.classList.remove('rotation');
				resizableElement.classList.remove('rotation');
			}
			resizableElement.classList.remove('redim');
			resizableElement.classList.remove('rognage');
			document.removeEventListener('mousemove', resizeElement);
			document.removeEventListener('mouseup', stopResize);
			document.removeEventListener('touchstart', resizeElement);
			document.removeEventListener('touchend', stopResize);
			sauvegarde();
		}
	} else {
		if (!lock) { // fonctions liés au cadenas
			redim=survolIntervignettes();  
		}

		if (redim===undefined && cible.classList.contains('vignette')){
			clicked=cible;
			clicked.style.borderColor='red';
			redim="background";
			vignette_background=cible.firstChild;
			cible.classList.add('vignette_selectionnee');
			let styles = window.getComputedStyle(cible.firstChild);
			backgroundPositionXdepart = extractNumericValue(styles.getPropertyValue("background-position-x"));
			backgroundPositionYdepart = extractNumericValue(styles.getPropertyValue("background-position-y"));
			posXDepartSouris=posX;
			posYDepartSouris=posY;      
		}
	}

	if (clicked){
		if (clicked.classList.contains('objet') || clicked.classList.contains('bubble') || clicked.classList.contains('onomatopee')){
		bouton_ancrer.disabled=false;
		if (clicked.classList.contains('ancre')){
			bouton_ancrer.classList.add('annuler');
			bouton_ancrer.title="Dés-ancrer l'objet";
		} else {
			bouton_ancrer.classList.remove('annuler');
			bouton_ancrer.title="Ancrer l'objet";
		}
		}
		else {bouton_ancrer.disabled=true;}
	} else {
		bouton_ancrer.disabled=true;
	}
}
  
function survolIntervignettes(){
    // Redimensionnement de la dernière bande
	if (liste_bandes.length > 0) {
		const rect = liste_bandes[liste_bandes.length-1].getBoundingClientRect();
		const mouseXrect =  posX - rect.left;
		const mouseYrect =  posY - rect.top;
		if (mouseYrect <= rect.height+borderWidth && mouseYrect>rect.height - borderWidth && mouseXrect>=0) {
		bande=liste_bandes[liste_bandes.length-1];    
		posYDepartSouris=posY;
		hauteur_bande=rect.height;
		return 'fin';
		} else {
			// Redimensionnement entre bandes
			if (liste_bandes.length>1){
				for (let k = 1; k < liste_bandes.length; k++) {
					const rect = liste_bandes[k].getBoundingClientRect();
					const mouseXrect =  posX - rect.left;
					const mouseYrect =  posY - rect.top;
				
					if (mouseYrect <= borderWidth && mouseYrect>-borderWidth && mouseXrect>=0){
						bande_haut=liste_bandes[k-1];
						bande_bas=liste_bandes[k];
						posYDepartSouris=posY;
						hauteur_bande_haut=bande_haut.offsetHeight;
						hauteur_bande_bas=bande_bas.offsetHeight;
						return 'v';
					}
				}
			}
			// Redimensionnement entre vignettes
			for (let i = 0; i < liste_vignettes.length; i++) {
				for (let j = 1; j < liste_vignettes[i].length; j++) {
		
					const rect = liste_vignettes[i][j].getBoundingClientRect();
					const mouseXrect =  posX - rect.left;
					const mouseYrect =  posY - rect.top;
				
					if ((mouseXrect < borderWidth && mouseXrect>-borderWidth)&&(mouseYrect >= 0 && mouseYrect < rect.height)){
						vignette_droite=liste_vignettes[i][j];
						vignette_gauche=liste_vignettes[i][j-1];
						posXDepartSouris=posX;
						largeur_vignette_droite=parseFloat(document.defaultView.getComputedStyle(vignette_droite).width, 10);
						largeur_vignette_gauche=parseFloat(document.defaultView.getComputedStyle(vignette_gauche).width, 10);
						return 'h';
					}
				}
			}
		}
	}
}

function move(event) {
    posX=event?.targetTouches?.[0]?.clientX || event?.clientX;
    posY=event?.targetTouches?.[0]?.clientY || event?.clientY;

    let survol = survolIntervignettes();

    if (survol==='h'){bd.style.cursor=ew_resize}
    else if (survol==='v' || survol==='fin'){bd.style.cursor=ns_resize}
    else {bd.style.cursor='default'}

    if (redim==='h'){
		diffX = posX - posXDepartSouris;
		if((largeur_vignette_droite - diffX) >= 60 && (largeur_vignette_gauche + diffX) >= 60){
			vignette_droite.style.width=largeur_vignette_droite - diffX + 'px';
			verifieBackground(vignette_droite);
			vignette_gauche.style.width=largeur_vignette_gauche + diffX + 'px';
			verifieBackground(vignette_gauche);
		} 
    } else if (redim==='v'){
		diffY = posY - posYDepartSouris;
		if((hauteur_bande_haut + diffY - 7) >= 40){
			bande_haut.style.height=hauteur_bande_haut + diffY - 7 + 'px';
		} else {
			bande_haut.style.height=40 + 'px';
		}
        let enfants = bande_haut.querySelectorAll('.vignette');
        enfants.forEach(enfant => {
          	verifieBackground(enfant);          
        });
		conteneur_bd.style.height = 'auto';
		const hauteur = conteneur_bd.getBoundingClientRect().height;
		conteneur_bd.style.height = hauteur + 'px';
    } else if (redim==='fin'){
        diffY = posY - posYDepartSouris;
        if((hauteur_bande + diffY - 7) >= 40){
          	bande.style.height=hauteur_bande/ratio + diffY/ratio + 'px';
        } else {
          	bande.style.height=40 + 'px';
        }
        let enfants = bande.querySelectorAll('.vignette');
        enfants.forEach(enfant => {
          	verifieBackground(enfant);          
        });
		conteneur_bd.style.height = 'auto';
		const hauteur = conteneur_bd.getBoundingClientRect().height;
		conteneur_bd.style.height = hauteur + 'px';
    } else if (redim==='background') {
		diffX = posX - posXDepartSouris;
		diffY = posY - posYDepartSouris;

		if (vignette_background.classList.contains('inversex')){diffX = -diffX;}
		if (vignette_background.classList.contains('inversey')){diffY = -diffY;}

		let nouvellePositionX=backgroundPositionXdepart-diffX;
		let nouvellePositionY=backgroundPositionYdepart-diffY;

		if (nouvellePositionX<0){
			vignette_background.style.backgroundPositionX='0%';
		} else if (nouvellePositionX>100){
			vignette_background.style.backgroundPositionX='100%';
		} else {        
			vignette_background.style.backgroundPositionX=nouvellePositionX+'%';
		}

		if (nouvellePositionY<0){
			vignette_background.style.backgroundPositionY='0%';
		} else if (nouvellePositionY>100){
			vignette_background.style.backgroundPositionY='100%';
		} else {
			vignette_background.style.backgroundPositionY=nouvellePositionY+'%';
		}
    }

    if (dragged) {  
        reclic=false;      
        event.preventDefault();
        opacite(dragged,0.3);
		dragged.style.left = posX - diffsourisx - corrx + "px";
       	dragged.style.top = posY - diffsourisy - corry + "px";	
    }
}

function release(event) {
    if (reclic){reclique(reclic);reclic=false}

	if (redim){sauvegarde();}
	redim=false;

	if (dragged) {
		opacite(dragged,1);
		let scrollBD=bd.scrollTop;
		if (dragged.classList.contains('fond')) {
			if (dragged.classList.contains('temporaire')){
				dragged.classList.remove('temporaire');
				let topValue = parseFloat(getComputedStyle(dragged).top)-decalage_bdy+scrollBD+5;
				let leftValue = parseFloat(getComputedStyle(dragged).left)-decalage_bd+5;        
				corrx=0;
				corry=0;
				if ( topValue<-50 || leftValue<-50 ){
					dragged.remove();
				} else {
					conteneur_bd.appendChild(dragged);
					dragged.style.top=topValue+'px';
					dragged.style.left=leftValue+'px';
					maj_credits(dragged);
				}
			}
			let vignette = verifieVignette();
			if (vignette){
				vignette.firstChild.style.backgroundImage='url('+(dragged.source).replace(/\/reduites/g, '')+')';
				//vignette.style.backgroundImage='url('+(dragged.source).replace(/\/reduites/g, '')+')';
				vignette.style.cursor='move';
				clicked=vignette;
				clicked.style.borderColor='red';
				clicked.classList.add('background');
				clicked.classList.add('vignette_selectionnee');
				vignette.classList.add(dragged.themecredit);
				maj_credits(vignette);
				maj_credits(dragged,'suppr');
				dragged.remove();
				dragged=null;          
			} else {
				dragged.classList.remove('dragged');      
				dragged=null;
			}
		} else {
			dragged.classList.remove('dragged');
			if (dragged.classList.contains('temporaire')){
				dragged.classList.remove('temporaire');
				dragged.classList.add('objet_galerie');
				let topValue = parseFloat(getComputedStyle(dragged).top)-decalage_bdy+scrollBD+5;
				let leftValue = parseFloat(getComputedStyle(dragged).left)-decalage_bd+5;          
				if ( topValue<-50 || leftValue<-50 ){
					dragged.remove();
				} else {
					conteneur_bd.appendChild(dragged);
					// dragged.style.top=topValue+'px';
					// dragged.style.left=leftValue+'px';
					maj_credits(dragged);              
					dragged=null;
				}
				corrx=0;
				corry=0;
				ancre(event);
			} else {
				if (dragged.classList.contains('cartouche')){
					let classes=verifie_magnetisme(dragged);
					if (classes.length>0){
						dragged.classList.add(...classes);
						if (dragged.classList.contains('cartouche_top') || dragged.classList.contains('cartouche_bottom')){dragged.style.top=null;dragged.style.bottom=null;}
						if (dragged.classList.contains('cartouche_left') || dragged.classList.contains('cartouche_right')){dragged.style.left=null;dragged.style.right=null;}
					}
				}
				if (dragged.classList.contains('ancre')){
					let stylesDragged=window.getComputedStyle(dragged);
					let stylesVignette=window.getComputedStyle(dragged.parentNode);
					let leftDragged=parseFloat(stylesDragged.left);
					let widthDragged=parseFloat(stylesDragged.width);
					let topDragged=parseFloat(stylesDragged.top);
					let heightDragged=parseFloat(stylesDragged.height);
					let widthVignette=parseFloat(stylesVignette.width);
					let heightVignette=parseFloat(stylesVignette.height);
					if (
						leftDragged - 20< - widthDragged ||
						topDragged - 20 < - heightDragged ||
						leftDragged + 20 > widthVignette ||
						topDragged + 20 > heightVignette
					) {
						ancre();
					}
				}
				dragged=null;
			}
		}
		sauvegarde();      
	}
}

function declique(mode,cible){
	if (mode==='clic'){
		if (clicked && !cible.classList.contains('keep_clic') && !cible.classList.contains('handle') && !cible.classList.contains('bouton_vignette')){
			showHandles(clicked,'none');
			clicked.style.borderColor=null;
			clicked.classList.remove('vignette_selectionnee');
			clicked.classList.remove('clicked');
			clicked.click=null;
			clicked=null;
			if (!cible.classList.contains('bouton_outil') && paragraphe_focus){paragraphe_focus.focus=null;paragraphe_focus=null;}
		}
	}
	else if (clicked){
		showHandles(clicked,'none');
		if (clicked.classList.contains('vignette')){
			clicked.style.borderColor=null;
			clicked.classList.remove('vignette_selectionnee');
		}
		clicked.classList.remove('clicked');
		clicked.click=null;
		clicked=null;
	}
}

function angleDeg(x,y,centreX,centreY){// Angle de départ
	// x = position horizontale de la souris
	// y = position verticale de la souris

	let diffCentreX = x - centreX;
	let diffCentreY = centreY - y;


	let angle_radian;
	let angle_degres;

	//On calcule l'angle par trignométrie d'après le vecteur absolu centre --> emplacement de la souris
	angle_radian = Math.atan(Math.abs(diffCentreX)/Math.abs(diffCentreY));
	angle_degres = angle_radian*180/Math.PI; // Conversion en degrés

	// Correctif, selon l'emplacment de la souris
	if (diffCentreY<0){
	if (diffCentreX>=0){angle_degres=180-angle_degres}              
	else {angle_degres=180+angle_degres;}
	} else if (diffCentreX<0) {
		angle_degres=360-angle_degres;
	}

	return angle_degres;
}
